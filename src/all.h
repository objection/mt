#pragma once

#include "../lib/darr/darr.h"
#include "../lib/ncmp/ncmp.h"
#include "../lib/print-table/print-table.h"
#include "../lib/rab/rab.h"
#include "../lib/time-string/time-string.h"
#include <argp.h>
#include <bits/posix2_lim.h>
#include <curl/curl.h>
#include <dirent.h>
#include <err.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <search.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <threads.h>
#include <unistd.h>
#include <yaml.h>

// These are the names of the various files. Change the strings
// to change their names.
#define $food_file "foods"
#define $hilarious_joke 1
#define $logfile "log"
#define $config_file "config"
#define $iso_date "%y-%m-%dT%H:%M:%S"

$make_arr (char *, strs);
$make_arr (char, str);
$make_arr (char, ints);

// Describes a "special_size", like "kingsize".
typedef struct special_size {
	char *name;
	double grams;
} special_size;
$make_arr (struct special_size, special_sizes);

// Describes any "nutrient". The name might not be good enough,
// because cals are included, here, and cals aren't a nutrient.
// Protien and fat, etc, will also be included, and they're not
// nutrients.
struct nutrient {

	// There's $max_nutrient_names for each nutrient. You can use the
	// alternatives in the config file, but you can't when using
	// --output. The reason you can't use it in --output is it's
	// annoying to program, a waste of time. I think now I shouldn't
	// have allowed multiple names in the config file, but I'll
	// leave it.
	char *name;
	double daily_goal;
};
#define NUTRIENT \
    /* At one point I tried to let you call Vitmains whatever you wanted, */ \
    /* basically  --  you know Vitamin B2 or Riboflavin. But it's ugly and */ \
    /* dull. So there's just one name, now. I'll leave the alternatives in */ \
    /* comments. */ \
    \
    /* CALS */ \
    x(N_CAL,            "cal",             get_cal_goal,          "energy_kcal") \
    \
    x(N_FIBRE,          "fibre",           get_fibre_goal,        "fiber") \
    x(N_INSOLUBLE_FIBRE,"insoluble-fibre", get_insol_fibre_goal,  "fiber_insoluble") \
    x(N_SUGARS,         "sugars",          get_sugars_goal,       "sugars") \
    x(N_ADDED_SUGAR,    "added-sugar",     get_added_sugar_goal, "sugars_added") \
    /* VITAMINS */ \
    /*  vitamin-b1, thiamine */   \
    x(N_VITAMIN_B1,     "thiamine",        get_b1_goal,           "vitamin_b1_thiamin") \
    /* vitamin-b2, riboflavin */ \
    x(N_VITAMIN_B2,     "b2",              get_b2_goal,           "vitamin_b2_riboflavin") \
    /* vitamin-b3, naicin */ \
    x(N_VITAMIN_B3,     "b3",              get_b3_goal,           "vitamin_b3_niacin") \
    /* vitamin-b5, panthothenic-acid */ \
    x(N_VITAMIN_B5,     "b5",              get_b5_goal,           "vitamin_b5_panthothenic_acid") \
    /* vitamin-b6, pyridoxine */ \
    x(N_VITAMIN_B6,     "b6",              get_b6_goal,           "vitamin_b6_pyridoxin") \
    /* vitamin-b7, biotin */ \
    /* x(N_VITAMIN_B7,     "b7",              get_b7_goal) */ \
    /* vitamin-b9, folate, folic-acid */ \
    x(N_FOLATE,         "folate",          get_folate_goal,        "folate_total") \
    x(N_VITAMIN_B9,     "b9",              get_b9_goal,            "folic_acid") \
    /* vitamin-b12, cobalamin */ \
    x(N_VITAMIN_B12,    "b12",             get_b12_goal,           "vitamin_b12_cobalamin") \
    /* vitamin-c, ascorbic-acid" */ \
    x(N_VITAMIN_C,      "c",               get_c_goal,             "vitamin_c_ascorbic_acid") \
    /* vitamin-a, retinol */ \
    x(N_VITAMIN_A,      "a",               get_a_goal,             "vitamin_a") \
    x(N_VITAMIN_A_IU,   "a-iu",            get_a_iu_goal,          "vitamin_a_iu") \
    \
    /* I had info about vitamin D before. It doesn't matter. Here we just */ \
    /* have "vitamin D", and that's it. */ \
    /* vitamin-d, vitamin-d2, ergocalciferol, */ \
    x(N_VITAMIN_D,      "d",               get_d_goal,             "vitamin_d_d2_d3_international_units") \
    x(N_VITAMIN_D_COLACALCIFEROL, \
                        "d-colacalciferol", \
                                           get_d_colacalciferolgoal, \
                                                                   "vitamin_d_cholacalciferol") \
                            /* vitamin-d3, cholecalciferol */ \
    /* vitamin-e, tocopherol */ \
    x(N_VITAMIN_E,       "e",               get_e_goal,            "vitamin_e_tocopherol") \
    x(N_OMEGA_3,         "omega-3",         get_omega_3_goal,      "omega-3_fatty_acids") \
    x(N_OMEGA_6,         "omega-6",         get_omega_6_goal,      "omega-6_fatty_acids") \
    /* vitamin-k, vitamin-k1, phylloquinone */ \
    \
    /* Vitamin K can be either k1 or k2. Two forms: K1 and K2. K1 is */ \
    /* phylloquinone; K2 is menaquinones. But K2 is made from by */ \
    /* bacteria in your gut, so we won't include it here. */ \
    /* x(N_VITAMIN_K,       "k",               get_k_goal) */ \
    /* x(N_BETACAROTENE,    "beta-carotene", get_beta_carotene_goal) */ \
    \
    /* MACRO NUTRIENTS */ \
    x(N_PROTEIN,        "protein",         get_protein_goal,       "protein") \
    x(N_FAT,            "fat",             get_fat_goal,           "fat") \
    x(N_MONOUNSAT,      "monounsaturated", get_monounsat_fat_goal, "monounsaturated_fatty_acids") \
    x(N_POLYUNSAT,      "polyunsaturated", get_polyunsat_fat_goal, "polyunsaturated_fatty_acids") \
    x(N_SAT_FAT,        "saturated",       get_sat_fat_goal,       "saturated_fat") \
	\
	/* FIXME: saturated fat and Food Repo's "fatty_acids_total_saturated" are surely */ \
	/* same thing. I think I really should combine them. */ \
    x(N_FATTY_ACIDS_TOTAL_SAT,  \
                        "fatty-acids-total-saturated", \
                                           get_fatty_acids_total_sat_goal, \
                                                                   "fatty_acids_total_saturated") \
    x(N_TRANS_FAT,      "trans",           get_trans_fat_goal,     "fatty_acids_total_trans") \
    x(N_CARBOHYDRATE,   "carbohydrate",    get_carb_goal,          "carbohydrates") \
    \
    /* MINERALS */ \
    /* MACRO MINERALS */ \
    x(N_SODIUM,         "sodium",          get_sodium_goal,        "sodium") \
    x(N_SALT,           "salt",            get_salt_goal,          "salt") \
    x(N_PHOSPHOROUS,    "phosphorous",     get_phosphorous_goal,   "phosphorus") \
    x(N_CALCIUM,        "calcium",         get_calcium_goal,       "calcium") \
    /* x(N_SULFUR,          "sulfur", get_sulfur_goal) */ \
    x(N_POTASSIUM,      "potassium",       get_potassium_goal,      "potassium_k") \
    x(N_MAGNESIUM,      "magnesium",       get_magnesium_goal,      "magnesium") \
    /* x(N_CHLORIDE,        "chloride", get_chloride_goal) */ \
    /* MICRO MINERALS */ \
    \
    /* x(N_IODINE,          "iodine", get_iodine_goal)  */  \
    x(N_ZINC,            "zinc",           get_zinc_goal,           "zinc") \
    /* x(N_MANGANESE,       "manganese", get_manganese_goal) */ \
    /* x(N_FLUORIDE,        "fluoride", get_fluoride_goal) */ \
    x(N_IRON,           "iron",            get_iron_goal,           "iron") \
    x(N_CHOLESTOROL,    "cholesterol",     get_cholestorol_goal,    "cholesterol") \
    x(N_COPPER,         "copper",          get_copper_goal,         "copper_cu") \
    /* x(N_CHROMIUM,        "chromium", get_chromium_goal) */ \
    /* x(N_SELENIUM,        "selenium", get_selenium_goal) */ \
    /* x(N_MOLYBDENUM,      "molybdenum", get_molybdenum_goal) */ \
    \
    x(N_NUTRIENTS,      0, 0, 0)

#define x(a, b, c, d) a,
enum nutrient_id { NUTRIENT };
#undef x
$make_static_arr (enum nutrient_id, nutrient_ids_static, N_NUTRIENTS);
extern struct nutrient nutrients[N_NUTRIENTS + 1];

// Describes a food.
typedef struct food {
	struct special_sizes special_sizes;

	// The array is parallel to "nutrients".
	double nutrient_vals_per_grams[N_NUTRIENTS];
} food;

enum meal_bit_type {
	MBT_FOOD,
	MBT_MEAL,
};
struct meal_bit {
	int item_idx;
	enum meal_bit_type type;
	int special_size_i;
	double multiplier;
};
$make_arr (struct meal_bit, meal_bits);

struct meal {
	struct meal_bits bits;
	double total;
};
enum item_type {
	IT_FOOD,
	IT_MEAL,
};

// This struct's a bit lame, I think. Well, what's lame is I have
// struct food and struct meal, and meals can contain foods, and then
// there's this struct item that can contain either. I think there
// should only be a struct food (or meal, whatever), that contains
// some combination of nutrients and foods (or meals). What we've got
// is a little more complex than it needs to be.
typedef struct item {
	char *name;
	union {
		struct food food;
		struct meal meal;
	};
	enum item_type type;
	bool failed;
	bool explanation_required;
} item;
$make_arr_with_member (struct item, items, int failed);

// Stuff you need to know about an entry got from the "log" file
// AKA the logfile.
//
// It means (? -- 2023-05-14T15:12:02+01:00) that this could instead
// contain a meal, now that we have meals. But it doesn't really
// matter.
typedef struct logfile_entry {
	struct ts_time time;

	struct item *item;

	// If you want you could replace these three fields with a meal
	// bit. But I'd rather drink bleach. Because I don't want to look
	// at this again. Having said that, I plan to do it once I've got
	// everything fixed up. Like, today (2023-05-14T21:50:44+01:00).
	struct meal_bit meal_bit;

	// If you scheduled a meal, this is when you did it. If not,
	// it's all 0s.
	struct ts_time scheduled_when;
} logfile_entry;
$make_arr (struct logfile_entry, logfile_entries);

enum args_flags {
	AF_LOG                  = 1 << 0,
	AF_PARSE_LOGFILE        = 1 << 1,
	AF_EDIT                 = 1 << 2,
	AF_LIST                 = 1 << 3,
	AF_REVIEW_WHEN_ADDING   = 1 << 4,
};

enum print_flags {
	RM_PRINT_TIME              = 1 << 0,
	RM_GROUP                   = 1 << 1,
	RM_TOTAL                   = 1 << 2,
	RM_PRINT_DAYS              = 1 << 3,
	RM_PRINT_WHEN_SCHED        = 1 << 4,
	RM_PRINT_PERCENT           = 1 << 5,
	RM_NO_OVERVIEW             = 1 << 6,
	RM_PRINT_INDEX             = 1 << 7,
	RM_HAVE                    = 1 << 8,
	RM_PORTIONS                = 1 << 9,
	RM_WARNING_THRESHOLDS      = 1 << 10,
	RM_WHEN_NEXT_CAN_EAT       = 1 << 11,
	RM_COUNTDOWN               = 1 << 12,
};

struct review_mods {

	// Not sure if this should be here. Not sure if review_mods should
	// even exist. Actually, I think it might have to. No, I'm not
	// going to check.
	double warning_thresholds[2];
	struct nutrient_ids_static fields;
	enum print_flags flags;
};

// These are args that you give on the commandline. One of them,
// default_grams can also be given in the config file.
struct args {
	double default_grams; // This can also be given in foods
	double bmr_adjustment;
	char *complete_string;
	enum args_flags flags;
	char *data_dir;
	char *profile_dir;
	char *review_str; // If this is set, we're in review mode.
	char *get_str;    // If this is set, we're in get mode.
	struct ts_time schedule;

	// This doesn't need to be malloced, but I'll use the 0 pointer
	// to tell if the filter's been given or not. Stuff needs to be
	// allocated to make regex anyway, so it's hardly a problem.
	regex_t *filter;
	struct review_mods review_mods;
};
// Args given on the cmdline.
extern struct args args;
// This struct contains values you set in the config file.
struct config {
	double bmr;

	// This are for printing. Perhaps you're not dealing with food
	// but, I dunno, time and space. In that case you could make your
	// "weight_unit" "hours" and your "energy_unit" "miles". Or if
	// you're counting discrete things you make these an empty string.
	char *weight_unit;
	char *energy_unit;
};

// Options you've set in the config file.
extern struct config config;

// Just the time right now, used all over the place.
extern struct ts_time now;

extern char error_buf[4096];

// Generating a comparison function for struct special_size.
$ncmp_dec_by_member_using_func (special_size, name, char *, ncmp_strs);

void strip (char *s);
int cmp_item_by_longest_name (const void *a, const void *b, int n_a);
int cmp_item_by_name (const void *a, const void *_b);
extern double (*get_nutrients_funcs[N_NUTRIENTS + 1])(void);
void nuke_item (struct item *item);
int cmp_nutrient_ids (void *a, void *b);
void open_in_editor (void);

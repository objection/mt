#pragma once

#include "all.h"

int review (char *range_str, struct logfile_entries logfile_entries,
		struct review_mods review_mods, struct items *items);

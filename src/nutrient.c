#define _GNU_SOURCE
#include "all.h"

double get_cal_goal (void) {
	if (config.bmr)
		return config.bmr + args.bmr_adjustment;
	return 0;
}
double get_b1_goal (void) {
	return 50;
}
double get_b2_goal (void) {
	return 50;
}
double get_b3_goal (void) {
	return 50;
}
double get_b5_goal (void) {
	return 50;
}
double get_b6_goal (void) {
	return 50;
}
double get_b7_goal (void) {
	return 50;
}
double get_b9_goal (void) {
	return 50;
}
double get_folate_goal (void) {
	return 50;
}
double get_b12_goal (void) {
	return 50;
}
double get_c_goal (void) {
	return 50;
}
double get_a_goal (void) {
	return 50;
}
double get_a_iu_goal (void) {
	return 50;
}
double get_d_goal (void) {
	return 50;
}
double get_d_colacalciferolgoal (void) {
	return 50;
}
double get_e_goal (void) {
	return 50;
}
double get_omega_3_goal (void) {
	return 50;
}
double get_omega_6_goal (void) {
	return 50;
}
double get_k_goal (void) {
	return 50;
}

double get_beta_carotene_goal (void) {
	return 50;
}

double get_protein_goal (void) {
	if (config.bmr)

		// The USDA dietary guidlines for Americans suggests
		// 		carbohydrate should make up 45-65% of daily
		// 				calories.
		// 		fat, 20-35%
		// 		protein, 10-35
		// These functions only return one value, not a range, so
		// we'll just be kind and choose the lower limit. I'll do this
		// for all. It will mean the combined values will never add up
		// to 1. Will that be a problem?
		return (config.bmr + args.bmr_adjustment) * 0.20;
	return 0;
}

double get_fat_goal (void) {
	if (config.bmr)

		// See note in get_protein_goal
		return (config.bmr + args.bmr_adjustment) * 0.10;
	return 0;
}
double get_carb_goal (void) {
	if (config.bmr)

		// See note in get_protein_goal
		return (config.bmr + args.bmr_adjustment) * 0.45;
	return 0;
}
double get_sodium_goal (void) {
	return 50;
}
double get_salt_goal (void) {
	return 50;
}
double get_phosphorous_goal (void) {
	return 50;
}
double get_calcium_goal (void) {
	return 50;
}
double get_sulfur_goal (void) {
	return 50;
}
double get_potassium_goal (void) {
	return 50;
}
double get_magnesium_goal (void) {
	return 50;
}
double get_chloride_goal (void) {
	return 50;
}
double get_iodine_goal (void) {
	return 50;
}
double get_zinc_goal (void) {
	return 50;
}
double get_manganese_goal (void) {
	return 50;
}
double get_fluoride_goal (void) {
	return 50;
}
double get_iron_goal (void) {
	return 50;
}
double get_copper_goal (void) {
	return 50;
}
double get_chromium_goal (void) {
	return 50;
}
double get_selenium_goal (void) {
	return 50;
}
double get_molybdenum_goal (void) {
	return 50;
}

double get_fibre_goal (void) {
	return 50;
}
double get_insol_fibre_goal (void) {
	return 50;
}

double get_sat_fat_goal (void) {
	return 50;
}
double get_fatty_acids_total_sat_goal (void) {
	return 50;
}
double get_trans_fat_goal (void) {
	return 50;
}

double get_sugars_goal (void) {
	return 50;
}

double get_added_sugar_goal (void) {
	return 50;
}

double get_polyunsat_fat_goal (void) {
	return 50;
}

double get_monounsat_fat_goal (void) {
	return 50;
}

double get_cholestorol_goal (void) {
	return 50;
}

#define x(a, b, c, d) c,
double (*get_nutrients_funcs[N_NUTRIENTS + 1])(void) = { NUTRIENT };
#undef x


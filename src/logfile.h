#pragma once

#include "all.h"

struct logfile_entries parse_logfile (struct items *items);
void write_logfile (struct logfile_entries logfile_entries);

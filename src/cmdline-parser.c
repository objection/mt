#define _GNU_SOURCE

#include "cmdline-parser.h"
#include "../lib/useful/useful.h"
#include "../lib/subopt/subopt.h"
#include "../lib/n-fs/n-fs.h"
#include "../lib/useful/useful.h"
#include "all.h"

// $get_finite_double uses isfinite, which is part of math.h.
// Probably, then useful should include math.h.
#include <math.h> // IWYU pragma: keep

enum long_opts {
	LO_GET = 2000,
	LO_PORTIONS,
	LO_REVIEW_WHEN_ADDING,
	LO_COMPLETE,
};

enum opt_flags {
	OF_FOOD_GIVEN = 1 << 0,
	OF_BAN_SCHED = 	1 << 1,
};

static enum opt_flags opt_flags;
static char *profile_dir_search = "default";

static int set_review_fields (char *arg) {
	char *value_s, *value_e = arg;
	struct subopt subopts[N_NUTRIENTS] = {};

	int index;
	$fori (j, N_NUTRIENTS)
		subopts[j].lng = nutrients[j].name;

	while ((index = so_get (&value_s, &value_e, 0, subopts,
					$n (subopts))) != SO_SUCCESS) {

		if (index < -1)
			errx (1, "Couldn't parse --output field \"%.*s\": %s",
					(int) (value_s - value_e), value_s, so_strerror (index));
		if (index != N_CAL)
			args.review_mods.fields.d[args.review_mods.fields.n++] = index;
		args.review_mods.flags |= RM_HAVE;
	}
	return 0;
}

static int set_warning_thresholds (char *arg) {
	auto review_mods = &args.review_mods;
	if (!strcmp ("0", arg) || !strcmp ("off", arg)) {
		args.review_mods.flags &= ~RM_WARNING_THRESHOLDS;
		return 0;
	}
	int rt = sscanf (arg, "%lf,%lf", &review_mods->warning_thresholds[0],
			&review_mods->warning_thresholds[1]);
	if (rt != 2)
		err (1, "Couldn't get two floats from \"-w%s\"", arg);

	if (review_mods->warning_thresholds[1] < review_mods->warning_thresholds[0]) {

		char *danger = strdup (arg);
		char *mild_discontent = strchr (danger, ',');
		*mild_discontent = 0;
		mild_discontent++;
		warnx ("In -w\"%s\", \"%s\" is <= \"%s\"; needs to be smaller",
				arg, danger, mild_discontent);
		free (danger);
		exit (1);
	}
	return 0;
}

static char *match_profile_dir (char *str) {
	DIR *d = opendir (".");
	if (!d)
		err (1, "Couldn't open your data dir %s", args.data_dir);
	struct dirent *de;
	errno = 0;
	struct stat sb = {};
	int n_dirs = 0;
	int a_dirs = 0;
	char **dirs = 0;
	while (de = readdir (d)) {
		if (*de->d_name == '.')
			continue;

		// readdir does not always correctly return DT_DIR, at least
		// it doesn't on 2022-03-21T19:01:21+00:00 on Fedora when
		// the directory it's reading is mouted with sshfs. So let's
		// just use stat. Very annoying bug, btw.
		lstat (de->d_name, &sb) != -1 ?:
			err (1, "\"%s\"", de->d_name);
		if (!(sb.st_mode & S_IFDIR))
			continue;

		u_grow (dirs, n_dirs, a_dirs, 8);
		dirs[n_dirs++] = strdup (de->d_name);
	}
	if (errno)
		err (1, "Error reading %s", args.data_dir);

	int rc;
	size_t n_match_idxes = -1;
	int match_idxes[n_dirs];
	rc = rab_get_completion_idxes (match_idxes, &n_match_idxes, str, n_dirs, dirs, 0);
	if (rc)
		errx (1, "rab_get_completion_idxes failed");

	if (!rab_have_just_one_match (n_match_idxes, n_dirs, dirs, str, "profiles", 0))
		exit (1);

	for (int i = 0; i < n_dirs; i++) {
		if (i != match_idxes[0])
			free (dirs[i]);
	}
	char *r = dirs[match_idxes[0]];
	free (dirs);
	if (closedir (d))
		err (1, "Couldn't close %s", args.data_dir);
	return r;
}

static int sort_out_end (void) {
	if (opt_flags & OF_BAN_SCHED && args.schedule.ts != -1
			&& args.schedule.ts > now.ts)
		errx (1, "\
You've tried to --schedule a a future food even though the previous you \
banned such behaviour");

	if (!args.data_dir) {
		args.data_dir = getenv ("XDG_DATA_HOME");
		if (!args.data_dir)
			args.data_dir = n_fs_get_full_path ("~/.local/share/mt");
	}

	// Dont try to make the share directory. It's useless
	// until you put foods in it, and you might want
	// remote-mount it.

	chdir (args.data_dir) == 0 ?:
		err (1, "Couldnt CD do data dir %s", args.data_dir);

	// Dont try to make the profile directory.

	// Match the profile directory. You're allowed to write,
	// eg "f" to match "food".
	//
	// We stick it in args.profile even though right
	// now (2022-03-21T17:53:29+00:00) it's not used out here.
	// But I'll do it anyway.
	args.profile_dir = match_profile_dir (profile_dir_search);
	if (!args.profile_dir)
		errx (1, "No profile dir whose initial chars are \"%s\"",
				profile_dir_search);

	return 0;
}

static int set_filter (char *arg) {
	if (args.filter) {
		regfree (args.filter);
		free (args.filter);
	}
	assert (args.filter = malloc (sizeof *args.filter));
	int rt = regcomp (args.filter, arg, 0);
	if (rt) {
		char buf[256];
		regerror (rt, args.filter, buf, 256);
		errx (1, "--filter: bad regex: %s", buf);
	}
	return 0;
}

static int set_schedule (char *arg) {
	args.schedule = ts_time_from_str (arg, 0, &now, 0);
	if (ts_errno)
		errx (1, "\"%s\" is bad time string", arg);
	args.flags |= AF_LOG;
	return 0;
}

static error_t eval_arg (int key, char *arg, struct argp_state *) {

								       /* How beautiful */
	switch (key) {
		case LO_GET:                                            args.get_str = arg; break;
		case LO_PORTIONS:                    args.review_mods.flags |= RM_PORTIONS; break;
		case LO_REVIEW_WHEN_ADDING:            args.flags |= AF_REVIEW_WHEN_ADDING; break;
		case LO_COMPLETE:                               args.complete_string = arg; break;
		case '%':                       args.review_mods.flags |= RM_PRINT_PERCENT; break;
		case 'a':                  !$get_finite_double (&args.bmr_adjustment, arg, 0)
						                 ?: errx (1, "\"%s\" is not a float", arg); break;
		case 'B':                                        opt_flags |= OF_BAN_SCHED; break;
		case 'C':                           args.review_mods.flags |= RM_COUNTDOWN; break;
		case 'd':                                              args.data_dir = arg; break;
		case 'D':                          args.review_mods.flags |= RM_PRINT_DAYS; break;
		case 'e':                                            args.flags |= AF_EDIT; break;
		case 'f':                                                 set_filter (arg); break;
		case 'G':                               args.review_mods.flags |= RM_GROUP; break;
		case 'i':                         args.review_mods.flags |= RM_PRINT_INDEX; break;
		case 'l':                                            args.flags |= AF_LIST; break;
		case 't':                          args.review_mods.flags |= RM_PRINT_TIME; break;
		case 'S':  args.review_mods.flags |= (RM_PRINT_TIME | RM_PRINT_WHEN_SCHED); break;
		case 'L':                                             args.flags |= AF_LOG; break;
		case 'N':                   args.review_mods.flags |= RM_WHEN_NEXT_CAN_EAT; break;
		case 'O':                         args.review_mods.flags |= RM_NO_OVERVIEW; break;
		case 'o':                                          set_review_fields (arg); break;
		case 'p':                                   args.flags |= AF_PARSE_LOGFILE; break;
		case 'P':                                         profile_dir_search = arg; break;
		case 'r':                                            args.review_str = arg; break;
		case 'R':                              args.review_str = "0dT00:00:00..0d"; break;
		case 's':                                               set_schedule (arg); break;
		case 'T':                               args.review_mods.flags |= RM_TOTAL; break;
		case 'w':                                     set_warning_thresholds (arg); break;
		case ARGP_KEY_ARG:                              opt_flags |= OF_FOOD_GIVEN; break;
		case ARGP_KEY_END:                                         sort_out_end (); break;

		default: break;
	}
	return 0;
}

char *make_help_msg_end (void) {
	char *r; size_t n_r;
	FILE *f = open_memstream (&r, &n_r);
	assert (f);
	fprintf (f, "\
Output total cals in MEALS ...\v\
MEALS is a list of \"meals\": \"rice beans potatoes\". \
You can specify a grams with a colon and a number, like \
this: \"rice:400g\". The \"g\" is just there for show. \
You can also specify portions in the \"" $food_file "\" \
file after the name and the cals-per-gram, like this: \"pack:32g\" \
in which case a MEAL can look like, eg, \"rice:handful\", or \
\"indomie:pack\". You can also specify a count after that, so the \
whole thing might look like \"meatball:single:5\". You ate five meatballs.\n\
\n\
The MEALS are read from a file in XDG_DATA_HOME/mt (by default, \
~/.local/share/mt/foods. Format: \
\"food cals-per-gram special-size:special-size-weight ...\". One of these per line. \
You can have as many special-size:special-size-weights as you like. \
You can also \
make aliases, like this: \"shroom alias:mushroom\". You have to have already \
defined \"mushroom\" in the usual way. Finally, you can set a --grams in the food \
file like this: default-grams 39427347342384.\n\
\n\
--list subtlety. By default it'll print calories per gram. If you'd \
rather see it per, eg, 100g, pass --grams=100g.\n\
\n\
A TIME RANGE is a string like 0s..1d or 2021-02-03..2021-02-02.\n\
\n\
A \"profile\" is, ultimately, a directory inside XDG_DATA_HOME/mt. It has \
a foods file and a logfile, same as the root dir. It's for keeping track \
of cigarettes or whatever. Why not? You'll need to make the dir yourself. \
And might want to -- say, it's cigarettes set default-grams to, eg, 1. \
The default profile is called \"default\"\n\
\n\
Here's all the \"nutrients\" this program deals with. They're also the values you can \
us with --output. You don't need to type the whole thing -- eg, \"beta\" will \
to do for \"beta-carotene\".\n");
	for (int i = 1; i < N_NUTRIENTS; i++)
		fprintf (f, "    %s\n", nutrients[i].name);
	assert (!fclose (f));
	return r;
}

int parse_cli (int argc, char **argv) {
	char *help_msg_end = make_help_msg_end ();
	struct argp argp = {
		.options = (struct argp_option []) {

			{0, 0, 0, 0, "Actions"},

			{"log", 'L', 0, 0,
				"Log your meal. Without this, mt is just food calculator"},
			{"review", 'r', "TIME RANGE", 1,
				"Print the log, filtering by TIME RANGE; see end"},
			{"review-today", 'R', 0, 0,
				"Shortcut of --review with a range the represents today"},
			{"schedule", 's', "TIME STRING", 0,
				"\
Log the meal but specify you didn't eat the food now but at TIME \
STRING (in the future) or past"},
			{"parse-logfile", 'p', 0, 0,
				"\
Parse logfile, spit it back out. For correcting mistakes. \
The logfile is always parsed an spat back out whenever you -L or -s"},
			{"list", 'l', 0, 0, "List foods and their calories. See end for subtlety"},
			{"edit", 'e', 0, 0,
				"Edit food file in EDITOR"},
			{"get", LO_GET, "FOOD", 0, "\
Search for FOOD in Food Repo. The food you select will be added to the selected \
profile's foods file. You probably want to pass --edit at the same time: this \
will open the foods file after so you can choose a name you like"},

			{0, 0, 0, 0, "Review modifiers"},

			{"bmr-adjustment", 'a', "REAL", 0,
				"\
An amount to modify your bmr by, for use with -R"},
			{"filter", 'f', "REGEX", 0,
				"A regex, to choose which foods to include, eg, \"rice|potatoes\". \
Regex flavour is PCRE. Only one of these can be specified, so use alternation like \
in the above example to select more than one food"},
			{"group", 'G', 0, 0,
			"With --review, group foods together. Eg, if you ate two meals of \
\"cals\" of 200 each, do \"cals 400 ...\" \
rather than \"cals 200\\ncals 200\""},
			{"no-overview", 'O', 0, 0,
				"Don't print the overview"},
			{"output", 'o', "SUBOPTS", 0,
				"\
With --review, the nutrients to see after \"name\", \"grams\" and \"cal\". \
See end for the list"},
			{"percent", '%', 0, 0,
				"\
Print percent consumed of nutrient goals (/limits) when printing"},
			{"print-days", 'D', 0, 0,
				"Interleave the date of each new day with output. With --total, \
print the date and then that day's total"},
			{"portions", LO_PORTIONS, 0, 0,
				"Print the a food's portions"},
			{"print-index", 'i', 0, 0,
				"Print the index in the first column"},
			{"print-time", 't', 0, 0,
				"\
With --review, also print the time the food was eaten (fourth column). If also -G, \
you'll just see the last time"},
			{"print-when-scheduled", 'S', 0, 0,
				"\
With --review, if a food was --scheduled, print when that was (in the fifth column). \
This also turns on --print-time (fourth column). Does nothing with -G"},
			{"print-when-next-can-eat", 'N', 0, 0,
			 	"Print the time when you can next eat"},
			{"print-countdown", 'C', 0, 0,
			 	"Print the duration between now and when you can next eat"},
			{"total", 'T', 0, 0,
	"With --print-days, print day dates and day; without, print only total"},
			{"warning-thresholds", 'w', "LIST", 0,
"LIST should be two floats seperated by a comma, like \"0.8,0.6\". The first \
float is the value above which to colourise the output red for danger; the \
second is the value above which to colourise the ouptut yellow for mild \
discontent. Values of \"0,0\", \"0\", and \"off\" turn off danger thresholds."},

			{0, 0, 0, 0, "General"},

			{"complete", LO_COMPLETE, "MEAL STR", 0,
				"\
Output a list of completions separated by spaces. \"choc\" might \
return \"chocolate\" and \"chocolate-buttons\". \"chocolate-buttons:l\" \
might return \"little\", and \"large\""},
		{"ban-scheduling", 'B', 0, 0,
				"\
Disallow you from scheduling. If you find yourself \"scheduling\" cakes (eating \
them now and saying you did later, getting into debt), you might want to make \
mt an alias to \"mt --ban-scheduling\". You can still, however, \"schedule\" \
a time in the past"},
		{"profile", 'P', "DIR", 0,
				"Use a different \"profile\" to the default; see end"},
			{"review-when-adding", LO_REVIEW_WHEN_ADDING, 0, 0,
				"\
When adding an item, print the output of -R at the same time. The usual \
review modifiers apply, like --total. It's not useful yet because if you put \
review modifiers in an alias, they'll apply even when you deliberately do \
-R, meaning you. This needs to take an options with a list of review-opts. \
But programming that is a pain"},
		{"data-dir", 'd', "DIR", 0,
				"\
Place where \"log\" and \"foods\" are kept (default: XDG_DATA_HOME or ~/.local/share/mt)"},
			{},
		},
		eval_arg,
		"MEAL[:grams[:n_portions]] ... ", help_msg_end,
	};

	argp_parse (&argp, argc, argv, 0, 0, 0);
	free (help_msg_end);
	return 0;
}

#pragma once

#include <yaml.h>

int get_event (struct yaml_event_s *pr, char *file, struct yaml_parser_s *parser);
int expect_event (struct yaml_event_s *pr, enum yaml_event_type_e event_type,
		char *file, struct yaml_parser_s *parser);
int expect_events (struct yaml_event_s *pr, enum yaml_event_type_e *event_types,
		char *file, struct yaml_parser_s *parser);
void yaml_err (struct yaml_event_s event, char *file, char *fmt, ...);

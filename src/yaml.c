#define _GNU_SOURCE

#include "yaml.h"
#include "../lib/useful/useful.h"
#include "../lib/rab/rab.h"
#include <stdarg.h>

// This seems pretty dumb, because they might change their API.
// Well, they won't change the enum.
char *event_strs[] = {
	[YAML_NO_EVENT] = "No event. Shouldn't be possible to see this, I say",
	[YAML_STREAM_START_EVENT] = "stream start",
	[YAML_STREAM_END_EVENT] = "stream start",
	[YAML_DOCUMENT_START_EVENT] = "document start",
	[YAML_DOCUMENT_END_EVENT] = "document end",
	[YAML_ALIAS_EVENT] = "alias",
	[YAML_SCALAR_EVENT] = "scalar",
	[YAML_SEQUENCE_START_EVENT] = "sequence start",
	[YAML_SEQUENCE_END_EVENT] = "sequence end",
	[YAML_MAPPING_START_EVENT] = "mapping start",
	[YAML_MAPPING_END_EVENT] = "mapping end",
};

static int wrong_event_type_err (struct yaml_event_s *got,
		enum yaml_event_type_e *wanted, char *file) {

	int n_wanted_strs = 0;
	char *wanted_strs[$n (event_strs)];
	for (int i = 0;; i++) {
		if (wanted[i] == 0)
			break;
		wanted_strs[n_wanted_strs++] = event_strs[wanted[i]];
	}
	char *got_str = event_strs[got->type];
	if (file)
		fprintf (stderr, "%s:", file);
	fprintf (stderr, "%zu:%zu: Expected ", got->start_mark.line,
			got->start_mark.column);
	rab_print_comma_list (n_wanted_strs, wanted_strs,
			&(struct rab_pcl_opts) {.and_str = "or",
			.f = stderr, .end = "\n"});
	fprintf (stderr, " but found %s\n", got_str);
	exit (1);
	return 0;
}

__attribute__((noreturn))
void yaml_err (struct yaml_event_s event, char *file, char *fmt, ...) {
	va_list ap; va_start (ap);
	if (file) fprintf (stderr, "%s:", file);
	fprintf (stderr, "%zu:%zu: ", event.start_mark.line, event.start_mark.column);
	vfprintf (stderr, fmt, ap);
	fputc ('\n', stderr);
	va_end (ap);
	exit (1);
}

int get_event (struct yaml_event_s *pr, char *file, struct yaml_parser_s *parser) {
	if (pr->type != YAML_NO_EVENT)
		yaml_event_delete (pr);
	yaml_parser_parse (parser, pr) ?:
		yaml_err (*pr, file, "\
Couldn't get event. Lines and columns are zero, as you see. Can't figure out how \
to get a decent error message from Libyaml");
	return 0;
}

int expect_event (struct yaml_event_s *pr, enum yaml_event_type_e event_type,
		char *file, struct yaml_parser_s *parser) {

	get_event (pr, file, parser);

	if (pr->type == event_type)
		return 0;
	wrong_event_type_err (pr, (enum yaml_event_type_e []) {event_type, 0}, file);
	return 1;
}

int expect_events (struct yaml_event_s *pr, enum yaml_event_type_e *event_types,
		char *file, struct yaml_parser_s *parser) {

	get_event (pr, file, parser);

	$each_sent (_, event_types) {
		if (pr->type == *_)
			return 0;
	}
	wrong_event_type_err (pr, event_types, file);
	return 1;
}



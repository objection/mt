#define _GNU_SOURCE

#include "lib/get-line/get-line.h"
#include "src/all.h"
#include "logfile.h"
#include "../lib/useful/useful.h"
#include <math.h> // IWYU pragma: keep

static int cmp_logfile_by_time (const void *_a, const void *_b) {
	const struct logfile_entry *a = _a; const struct logfile_entry *b = _b;
	return a->time.ts - b->time.ts;
}

static bool is_empty_line (char *str) {
	while (*str) {
		if (*str == '\n') break;
		if (!isspace (*str)) return 0;
		str++;
	}
	return 1;
}

// Reads the logfile.
//
// LOGFILE STRUCTURE:
// 		date name grams cals
// 		date name grams cals
// 		...
// 		total: whatever
//
// Example
// 		21-02-02T00:00:00 indomie 80.00g 400.00c
// 		21-02-02T00:00:00 eat-natural-bar 50.00g 238.00c
//		total: 130.00g 638.00c
//
// The parsing is with strtok, so the field seperator must be a space,
// not a tab.
//
// The cals are ignored when parsing. They're just there for you if
// you want to use the cals for anything. Truthfully, I was going to
// use this logfile in a script. However I've just put in --review
// which lets you match entries by date. So I should get rid of the
// calories. But I won't.
struct logfile_entries parse_logfile (struct items *items) {
	struct logfile_entries r = {};
	struct gl_line gl = make_line ($logfile, 0);
	if (!gl.f) return (struct logfile_entries) {0};
	double val;
	int line = 0;
	while (line++, get_line_strip (&gl) != -1) {

		if (!strncmp (gl.buf, "total", 5))
			continue;
		if (is_empty_line (gl.buf))
			continue;
		// This is not very flexible parsing.
		struct logfile_entry entry = {};
		char *p;

#define $chop_str_or(_what) \
		if (!(p = strsep (&p_buf, " \t"))) { \
				_what; \
		}
#define $f_pos "%s/%s/log:%d: "
#define $f_pos_args args.data_dir, args.profile_dir, line
#define could_not_get_date() \
		({ errx (1, $f_pos "Couldn't get time from %s", $f_pos_args, p); })

		// You need this for strsep, which is not a great function.
		char *p_buf = gl.buf;
		if (!(p = strsep (&p_buf, " \t")))
			could_not_get_date ();

		entry.time = ts_time_from_str (p, 0, &now, TS_MATCH_ONLY_ISO);
		if (ts_errno)
			could_not_get_date ();

		$chop_str_or (errx (1, $f_pos "Couldn't get food from %s",
					$f_pos_args, p));

		entry.item = arr_find (items, &(struct item) {.name = p},
				cmp_item_by_name);
		if (!entry.item)
			errx (1, $f_pos "You haven't defined \"%s\" in your \"foods\" file",
					$f_pos_args, p);

		$chop_str_or (errx (1, $f_pos "Couldn't get food or meal size from %s",
					$f_pos_args, p));

		entry.meal_bit.special_size_i = -1;
		if (entry.item->type == IT_FOOD) {
			if (strcmp (p, "default")) {
				arr_each (&entry.item->food.special_sizes, _) {
					if (!strcmp (p, _->name))
						entry.meal_bit.special_size_i = _ - entry.item->food.special_sizes.d;
				}
				if (entry.meal_bit.special_size_i == -1)
					errx (1, $f_pos "\"%s\" isn't a valid size", $f_pos_args, p);
			}
		} else if (strcmp (p, "meal"))
			errx (1, $f_pos "\
\"%s\" is a meal, so the field after its name should \"meal\", not \"%s\"",
$f_pos_args, entry.item->name, p);

		$chop_str_or (errx (1, $f_pos "Couldn't get multiplier from %s",
					$f_pos_args, p));

		if ($get_finite_double (&val, p, 0))
			errx (1, $f_pos "Couldn't read multiplier float from: \"%s\"",
					$f_pos_args, p);

		entry.meal_bit.multiplier = val;
		if (p = strsep (&p_buf, " \t")) { \
			entry.scheduled_when = ts_time_from_str (p, 0, &now, TS_MATCH_ONLY_ISO);
			if (ts_errno)
				could_not_get_date ();
		}

		// The unit suffix (value of config file "energy_unit",
		// default "c" is ignored.
		arr_push (&r, entry);

	}
	!nuke_line (&gl) ?: err (1, "Couldn't close %s", gl.path);
	if (r.n)
		qsort (r.d, r.n, sizeof *r.d, cmp_logfile_by_time);
	return r;
}

void write_logfile (struct logfile_entries logfile_entries) {

#if 0
	double *all_time_totals = get_all_time_totals (logfile_entries);
#endif

	auto f = fopen ("log", "w");
	if (!f) err (1, "Couldn't open %s/log", args.data_dir);

	size_t n_buf = 32;
	char buf[n_buf];
	arr_each (&logfile_entries, entry) {
		assert (ts_str_from_ts (buf, n_buf, entry->time.ts,
					$iso_date));

		auto item = entry->item;
		char *special_size_name =
			item->type == IT_MEAL ?
				"meal"
			: entry->meal_bit.special_size_i == -1 ?
				"default"
			:   item->food.special_sizes.d[entry->meal_bit.special_size_i].name;
		fprintf (f, "%s %s %s %f",
				buf, item->name, special_size_name, entry->meal_bit.multiplier);
		if (entry->scheduled_when.ts) {
			assert (ts_str_from_ts (buf, n_buf, entry->scheduled_when.ts,
						$iso_date));
			fprintf (f, " %s", buf);
		}

		fputc ('\n', f);
	}
}

#define _GNU_SOURCE

#include <stdio.h>
#include <linux/limits.h>
#include "src/foods.h"
#include "../lib/useful/useful.h"
#include "yaml.h"
#include "meal.h"

#define $assert_impossible() assert (!"Should be impossible")
#define $expect_event(_event_type) \
	expect_event (&event, _event_type, file_full_path, &parser);
#define $expect_events(_event_types) \
	expect_events (&event, _event_types, file_full_path, &parser);
#define $parse_foods_err(_fmt, ...) \
	yaml_err (event, file_full_path, _fmt, ##__VA_ARGS__);
#define $key_is(_what) \
	$strmatch ((char *) event.data.scalar.value, _what)

// This static is needed? Without this, gcc complains.
static struct yaml_parser_s parser;
static struct yaml_event_s event;

char file_full_path[PATH_MAX];

static int cmp_nutrient_by_name (const void *a, const void *b) {
	return strcmp (((struct nutrient *) a)->name, ((struct nutrient *) b)->name);
}

static void parse_foods_invalid_nutrient_err (char *str) {
	fprintf (stderr, "%s:%zu:%zu: \"%s\" isn't a valid nutrient. Valid ones are: ", file_full_path,
			event.start_mark.line, event.start_mark.column, str);
	char *nutrient_common_names[N_NUTRIENTS];
	$fori (i, N_NUTRIENTS)

		// We don't bother with the alternative names.
		nutrient_common_names[i] = nutrients[i].name;

	rab_print_comma_list (N_NUTRIENTS, nutrient_common_names,
			 &(struct rab_pcl_opts) {.end = "\n", .f = stderr});
	exit (1);
}

static int foods_parse_meal_bit (struct meal *meal, char *name, struct items *items) {

	$expect_events (((enum yaml_event_type_e []) {

		// Like
		// 	pot-noodle:
		// 		size, etc etc
		YAML_MAPPING_START_EVENT,

		// Like just
		// 	pot-noodle:
		// 		// nothing, meaning a pot nooode, the default size,
		// 		// default amount.
		YAML_SCALAR_EVENT,
		0,
	}));
	char *multiplier = 0, *multiplier_or_special_size = 0;
	if (event.type == YAML_MAPPING_START_EVENT) {
		while (1) {
			$expect_events (((enum yaml_event_type_e []) {
				YAML_SCALAR_EVENT, YAML_MAPPING_END_EVENT, 0,
			}));
			if (event.type == YAML_MAPPING_END_EVENT)
				break;

			// Obviously, the way I've done this means that duplicate
			// fields will be silently updated.
			char key[BUFSIZ];
			strncpy (key, (char *) event.data.scalar.value, BUFSIZ);
			$expect_event (YAML_SCALAR_EVENT);
			char *val = (char *) event.data.scalar.value;
#define $pmb_add(_it) ({ $nuke_if (_it); _it = strdup (val); })
			if ($strmatch (key, "size"))        $pmb_add (multiplier_or_special_size);
			else if ($strmatch (key, "amount")) $pmb_add (multiplier);
			else                                $parse_foods_err ("\
	\"%s\" is not a valid meal thing. Valid are \"amount\" and \"size\"",
	key);
		}
	}
	!set_meal (meal, name, multiplier_or_special_size, multiplier, 0, items) ?:
		$parse_foods_err ("%s", error_buf);
	$nuke_if (multiplier);
	$nuke_if (multiplier_or_special_size);
	return 0;
}

static int parse_contains (struct meal *meal, struct items *items) {
	$expect_event (YAML_MAPPING_START_EVENT); // ":"
	while (1) {
		$expect_events (((enum yaml_event_type_e []) {
			YAML_MAPPING_END_EVENT, YAML_SCALAR_EVENT, 0,
		}));
		if (event.type == YAML_MAPPING_END_EVENT)
			break;

		char name[BUFSIZ];
		strcpy (name, (char *) event.data.scalar.value);
		foods_parse_meal_bit (meal, name, items);
	}
	return 0;
}

static int parse_nutrient (struct food *food) {

	char *key = strdup ((char *) event.data.scalar.value);
	$expect_event (YAML_SCALAR_EVENT);
	char *val = (char *) event.data.scalar.value;
	struct nutrient *nutrient = u_find ((struct nutrient) {.name = key},
			nutrients, N_NUTRIENTS, cmp_nutrient_by_name);
	if (!nutrient)
		parse_foods_invalid_nutrient_err (key);
	$nuke_if (key);

	int nutrient_idx = nutrient - nutrients;
	sscanf (val, "%lf", &food->nutrient_vals_per_grams[nutrient_idx]) == 1 ?:
		$parse_foods_err ("Couldn't get cals from \"%s\"", val);
	return 0;
}

static int parse_size (struct special_size *pr) {
	*pr = (struct special_size) {};
	$expect_events (((enum yaml_event_type_e []) {
			YAML_SCALAR_EVENT, YAML_MAPPING_END_EVENT, 0}));
	if (event.type == YAML_MAPPING_END_EVENT)
		return 1;

	pr->name = strdup ((char *) event.data.scalar.value);
	$expect_event (YAML_SCALAR_EVENT);
	char *val = (char *) event.data.scalar.value;
	if (sscanf (val, "%lf", &pr->grams) != 1)
		$parse_foods_err ("Couldn't get specials size's from \"%s\"", val);
	return 0;
}

static int parse_sizes (struct item *item) {
	$expect_event (YAML_MAPPING_START_EVENT);
	while (1) {
		struct special_size special_size;
		parse_size (&special_size) == 0 ?: ({
			return 1;
		});
		arr_push (&item->food.special_sizes, special_size);
	}
	return 0;
}

static int parse_explanation_required (struct item *item) {
	$expect_event (YAML_SCALAR_EVENT);
	char *val = (char *) event.data.scalar.value;

	// The Yaml doesn't seem to convert its values into types.
	if (!strcasecmp (val, "true") || !strcasecmp (val, "1") || !strcasecmp (val, "yes"))
		item->explanation_required = 1;
	else if (!strcasecmp (val, "false") || !strcasecmp (val, "0") ||
			!strcasecmp (val, "no"))
		item->explanation_required = 1;
	return 0;
}

static struct item parse_item (struct items *items) {

	$expect_events (((enum yaml_event_type_e []) {
		YAML_SCALAR_EVENT,
		YAML_MAPPING_END_EVENT,
		0
	}));
	if (event.type == YAML_MAPPING_END_EVENT)
		return (struct item) {.failed = 1};

	struct item r = {.type = IT_FOOD};

	r.name = strdup ((char *) event.data.scalar.value);

	$expect_event (YAML_MAPPING_START_EVENT);
	while (1) {
		if (!yaml_parser_parse (&parser, &event))
			assert (0);

		switch (event.type) {

			case YAML_SCALAR_EVENT: {
                if ($key_is ("sizes"))            parse_sizes (&r);
                else if ($key_is ("contains")) {

					// Yeah, an item becomes a meal if it's got a
					// contains. There's no checking to see if you've
					// done "sizes" (which would mean it's a food)
					// before "contains".
					parse_contains (&r.meal, items);
					r.type = IT_MEAL;
				} else if ($key_is ("explanation-required"))
					parse_explanation_required (&r);
                else                              parse_nutrient (&r.food);
			}
			break;
			case YAML_MAPPING_END_EVENT:
				goto out;
			break;

			default: assert (0);
		}
	}
out:
	if (r.failed) nuke_item (&r);

	return r;
}

struct items parse_foods (void) {
	struct items r = {};
	yaml_parser_initialize (&parser) ?: errx (1, "Couldn't initialise yaml parser");

	sprintf (file_full_path, "%s/%s/%s", args.data_dir, args.profile_dir, $food_file);

	FILE *f = fopen (file_full_path, "r");
	if (!f) err (1, "Couldn't open foods file %s", file_full_path);

	yaml_parser_set_input_file (&parser, f); // Doesn't return anything

	$expect_event (YAML_STREAM_START_EVENT);
	$expect_event (YAML_DOCUMENT_START_EVENT);
	$expect_event (YAML_MAPPING_START_EVENT);

	for (;;) {
		auto item = parse_item (&r);
		if (item.failed) break;
		arr_push (&r, item);
	}

	yaml_event_delete (&event);
	yaml_parser_delete (&parser);
	fclose (f);
	return r;
}

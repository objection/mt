#define  _GNU_SOURCE

#include <stdio.h>
#include <assert.h>
#include "all.h"
#include "foods.h"
#include "review.h"
#include "logfile.h"
#include "meal.h"
#include "config-parser.h"
#include "food-repo.h"
#include "cmdline-parser.h"
#include "../lib/useful/useful.h"

static void list_nutrients (struct items *) {
		// Taken this out. Obviously we have multiple
		// nutrients now, which means we'd want to
		assert (!"I've taken this out for now");
#if 0

		arr_each (items, _) {
			printf ("%s: ", _->name);
			switch (_->type) {
				case IT_FOOD:
					break;
				case IT_MEAL:
					break;
			}
			$each (_, _->nutrient_vals_per_grams, N_NUTRIENTS)
				printf ("%.2f ", *_);
			fputc ('\n', stdout);
			(opt_flags & OF_DEFAULT_GRAMS_GIVEN
			 ? args.default_grams
			 : 1));
		}
		exit (0);
#endif
}

char **get_item_name (void *obj, void *) { return &((struct item *) obj)->name; }

char **get_special_size_name (void *obj, void *) { return &((struct special_size *) obj)->name; }

[[noreturn]]
static void complete (struct items *items) {
	char *food = 0, *multiplier_or_special_size = 0, *multiplier = 0;
	chop_meal_str (&food, &multiplier_or_special_size, &multiplier, args.complete_string);
	if (multiplier) {
		puts ("");
		exit (0);
	}

#define $print_no_match_and_exit() do { puts (""); exit (1); } while (0)
	size_t n_food_match_idxes = -1;
	int food_match_idxes[items->n];
	memset (food_match_idxes, 0, sizeof *food_match_idxes * items->n);
	if (food) {
		assert (food);
		n_food_match_idxes = rab_get_completion_idxes (food_match_idxes,
				&n_food_match_idxes, food, items->n, items->d,
				&(struct rab_getter) {get_item_name, sizeof (struct item)});
	}
	if (multiplier_or_special_size) {
		if (n_food_match_idxes > 1) {

			// FIXME: we need the exact match for matching special sizes.
			// Like, say you have "aero:" and you try to complete. Without
			// knowing that you've got an exact match, the
			// special size-printing code will fail
			$fori (i, n_food_match_idxes) {
				if (!strcmp (items->d[food_match_idxes[i]].name, food)) {
					food_match_idxes[0] = food_match_idxes[i];
					n_food_match_idxes = 1;
					break;
				}
			}
		}
		if (n_food_match_idxes != 1)
			$print_no_match_and_exit ();

		size_t n_mult_matches = -1;

		auto item = items->d + food_match_idxes[0];

		// Luckily meals can't have special sizes. They should be
		// able to. They don't, I guess, because it's tricky to
		// program. But that makes this easier to program.
		if (item->type != IT_FOOD)
			$print_no_match_and_exit ();
		int mult_match_idxes[item->food.special_sizes.n];
		memset (mult_match_idxes, 0,
				item->food.special_sizes.n * sizeof *mult_match_idxes);
		rab_get_completion_idxes (mult_match_idxes, &n_mult_matches,
												multiplier_or_special_size,
												item->food.special_sizes.n,
												item->food.special_sizes.d,
												&(struct rab_getter) {
													.func = get_special_size_name,
													sizeof (struct special_size)
												});
		u_each (_, n_mult_matches, mult_match_idxes)
			printf ("%s:%s ", item->name, item->food.special_sizes.d[*_].name);
		puts ("");
	} else {
		$fori (i, n_food_match_idxes) {
			printf ("%s", items->d[food_match_idxes[i]].name);
			if (i != n_food_match_idxes - 1)
				putchar (' ');
		}
		putchar ('\n');
	}
	exit (0);
}

int main (int argc, char **argv) {
	now = ts_time_from_ts (time (0));
	parse_cli (argc, argv);
	chdir (args.profile_dir) == 0 ?:
		err (1, "Couldnt CD to profile %s/%s", args.data_dir, args.profile_dir);

	if (args.get_str) {
		get_from_food_repo (args.get_str);
		exit (0);
	}

	auto items = parse_foods ();
	if (args.complete_string)
		complete (&items);

	if (items.failed) {
		err (1, "\
Failed to parse foods. However, I think an error message should have been \
given earlier. We should just be exiting here");
		exit (1);
	}

	parse_config_file ();

	auto logfile_entries = parse_logfile (&items);

	if (args.flags & AF_LIST)               list_nutrients (&items);
	else if (args.flags & AF_PARSE_LOGFILE) {}
	else if (args.review_str)               review (args.review_str, logfile_entries,
													args.review_mods, &items);
	else if (args.flags & AF_EDIT)          open_in_editor ();
	else {

		struct item item;
		int meals_parsed = parse_meal (&item, argc - 1, argv + 1, 0, 1, &items);
		if (!meals_parsed)
			errx (1, "You specify any MEALS");
		push_meal_to_logfile_entries (&logfile_entries, item.meal, &items);

        if (args.flags & AF_LOG) {
			write_logfile (logfile_entries);
			if (args.flags & AF_REVIEW_WHEN_ADDING)
				review ("0dT00:00:00..1s", logfile_entries, args.review_mods, &items);
		} else
			review ("0s..1s", logfile_entries, args.review_mods, &items);
	}

	exit (0);
}


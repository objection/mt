#include <stdio.h>
#define _GNU_SOURCE

#include "lib/str-to-int-arr/str-to-int-arr.h"
#include "src/meal.h"
#include <ctype.h>
#include "../lib/useful/useful.h"
#include "all.h"
#include "review.h"
#include <yaml.h>
#include "food-repo.h"
#include "yaml.h"

#define x(a, b, c, d) d,
char *food_repo_nutrient_names[N_NUTRIENTS + 1] = { NUTRIENT };
#undef x

#define $expect_event(_event_type) \
	expect_event (&event, _event_type, 0, &parser);
#define $expect_events(_event_types) \
	expect_events (&event, _event_types, 0, &parser);
#define $key_is(_what) \
		$strmatch ((char *) event.data.scalar.value, _what)
struct review_mods review_mods = {
	.flags = RM_NO_OVERVIEW | RM_PRINT_INDEX | RM_PORTIONS,
	.fields = {
		.d = {N_CAL}, .n = 1,
	},
};

// Without this static gcc complains. What's going on?
static struct yaml_parser_s parser;
static struct yaml_event_s event;

static size_t curl_write_func (char *ptr, size_t size, size_t n_memb,
		void *user_data) {
	FILE *f = user_data;
	int rt = fwrite (ptr, size, n_memb, f);
	if (rt != n_memb / size) {
		if (errno)
			assert (0);
	}
	return rt;
}

static struct curl_slist *make_curl_slist (char **headers) {
	if (!headers || !*headers) return 0;
	struct curl_slist *r = curl_slist_append (0, *headers);
	if (!headers[0]) return r;
	for (char **_ = headers + 1; *_; _++) {
		assert (curl_slist_append (r, *_));
	}
	return r;
}

static char *get_food_repo_response_as_string (char *food) {

	char *r; size_t n_r;
	FILE *curl_f = open_memstream (&r, &n_r);
	char curl_error_buf[CURL_ERROR_SIZE];

#define $food_repo_api_key "6ded019d933fdfd25af8a1ce13c5af5e"

	CURL *curl = curl_easy_init ();
	if (!curl) err (1, "Couldn't initialise curl");

#define $doit(action, arg) \
	assert (!curl_easy_setopt (curl, action, arg))

	$doit (CURLOPT_URL, "\
https://www.foodrepo.org/api/v3/products/_search?api_key=" $food_repo_api_key);
	struct curl_slist *headers = make_curl_slist ((char *[]) {
			"Content-Type: application/vnd.api+json",
			"Accept: application/json",
			"Authorization: Token token=" $food_repo_api_key,
			(char *) 0
		});
	$doit (CURLOPT_HTTPHEADER, headers);
	$doit (CURLOPT_POST, 1);
	$doit (CURLOPT_ERRORBUFFER, curl_error_buf);
	$doit (CURLOPT_WRITEFUNCTION, curl_write_func);
	$doit (CURLOPT_WRITEDATA, curl_f);

	char fields[BUFSIZ];
	snprintf (fields, BUFSIZ, "\
{ \
	\"_source\": { \
		\"includes\": [ \
			\"name_translations.en\", \
			\"nutrients.*.per_hundred\", \
			\"quantity\" \
		] \
	}, \
	\"query\": { \
		\"bool\": { \
			\"must\": [ \
				{ \
					\"exists\": { \
						\"field\": \"nutrients.*.per_hundred\" \
					} \
				}, \
				{ \
					\"exists\": { \
						\"field\": \"name_translations.en\" \
					} \
				}, \
				{ \
					\"match\": { \
						\"name_translations.en\": { \
							\"operator\": \"AND\", \
							\"fuzziness\": 2, \
							\"query\": \"%s\" \
						} \
					} \
				} \
			] \
		} \
	} \
}",
	food);
	$doit (CURLOPT_POSTFIELDS, fields);

	if (curl_easy_perform (curl))
		errx (1, "Couldn't get food from Food Repo: %s", curl_error_buf);

	curl_easy_cleanup (curl);
	curl_slist_free_all (headers);
	fclose (curl_f);
	/* printf ("%s\n", r); */
	/* exit (0); */

	return r;
}

static int parse_nutrient (struct item *item) {
	$expect_events (((enum yaml_event_type_e []) {
		YAML_SCALAR_EVENT,
		YAML_MAPPING_END_EVENT,
		0}));        // Nutrient name
	if (event.type == YAML_MAPPING_END_EVENT)
		return 1;
	char name[BUFSIZ];
	strncpy (name, (char *) event.data.scalar.value, BUFSIZ);
	char **match = u_find (name, food_repo_nutrient_names, N_NUTRIENTS, ncmp_strs);
	enum nutrient_id nutrient_id = match ? match - food_repo_nutrient_names : -1;
	$expect_event (YAML_MAPPING_START_EVENT); // open curly

	$expect_event (YAML_SCALAR_EVENT);        // "per_hundred"

	$expect_event (YAML_SCALAR_EVENT);        // "per_hundred's" value

	if (!match && !$strmatch (name, "energy")) {
		printf ("\"%s\" not dealt with\n", name);
		exit (1);
	} else if (match) {

		double *val = item->food.nutrient_vals_per_grams + nutrient_id;

		assert (!$get_double (val,
				 (char *) event.data.scalar.value, 0));

		// It it's the first time we've see this nutrient, add it
		// to args.output_nutrients
		if (nutrient_id != N_CAL &&
				*val != 0 &&
				u_find (&nutrient_id, review_mods.fields.d, review_mods.fields.n,
					cmp_nutrient_ids))
			review_mods.fields.d[review_mods.fields.n++] = nutrient_id;

		// They give values per hundred; we do it per gram.
		*val /= 100;
	}
	$expect_event (YAML_MAPPING_END_EVENT);   // close curly
	return 0;
}

bool is_valid_identifier_char (char _char) {
	return isalpha (_char);
}

static char *make_name (char *food_repo_name) {

	// Just malloc a bunch.
	char *r = calloc (strlen (food_repo_name) * 2, sizeof *r);
	char *q = r;
	while (isspace (*food_repo_name)) food_repo_name++;
	$each_sent (p, food_repo_name) {
		if (!is_valid_identifier_char (*p)) {
			while (!is_valid_identifier_char (*p)) {
				p++;
			}
			*q++ = '-';
		}
		*q++ = tolower (*p);
	}
	return r;
}

static int parse_food_repo_food (struct item *pr) {

	*pr = (struct item) {.type = IT_FOOD};

	$expect_events (((enum yaml_event_type_e []) {
		YAML_MAPPING_START_EVENT, YAML_SEQUENCE_END_EVENT,
		0
	})); // open curly or the end of the "hits" sequence.
	if (event.type == YAML_SEQUENCE_END_EVENT)
		return 1;

	$expect_event (YAML_SCALAR_EVENT);        // "_score"
	$expect_event (YAML_SCALAR_EVENT);        // "_score's" value
	$expect_event (YAML_SCALAR_EVENT);        // "_source"
	$expect_event (YAML_MAPPING_START_EVENT); // open curly

	$expect_event (YAML_SCALAR_EVENT);        // "quantity"
	$expect_event (YAML_SCALAR_EVENT);        // "quantity"
	double val;
	assert (!$get_double (&val,
			(char *) event.data.scalar.value, 0));
	arr_add (&pr->food.special_sizes, ((struct special_size) {.name = "unknown",
				.grams = val}));

	$expect_event (YAML_SCALAR_EVENT);        // "name_translations"
	$expect_event (YAML_MAPPING_START_EVENT); // open curly

	$expect_event (YAML_SCALAR_EVENT);        // "en"
	$expect_event (YAML_SCALAR_EVENT);        // The English name

	pr->name = make_name ((char *) event.data.scalar.value);

	$expect_event (YAML_MAPPING_END_EVENT);   // close curly

	$expect_event (YAML_SCALAR_EVENT);        // "nutrients"
	$expect_event (YAML_MAPPING_START_EVENT); // open curly
	while (1)
		!parse_nutrient (pr) ?: ({ break; });
	$expect_event (YAML_MAPPING_END_EVENT);
	$expect_event (YAML_MAPPING_END_EVENT);
	return 0;
}

static struct items get_food_repo_response (char *food) {
	struct items r = {};
	yaml_parser_initialize (&parser) ?: errx (1, "Couldn't initialise yaml parser");

	// It'd be nice to not get it as a string. Curl outputs to
	// stdout by default. So I could make a pipe?
	char *str = get_food_repo_response_as_string (food);

	yaml_parser_set_input_string (&parser,

			// FIXME: don't cast?
			(const unsigned char *) str,
			strlen (str)); // Doesn't return anything

	$expect_event (YAML_STREAM_START_EVENT);
	$expect_event (YAML_DOCUMENT_START_EVENT);
	$expect_event (YAML_MAPPING_START_EVENT);

	// Just loop until we get to the "hits" field. Right now we're
	// not checking for correctness. Who cares?
	while (1) {
		get_event (&event, 0, &parser);

		// FIXME: deal with error, perhaps in get event. Perhaps deal
		// with "failed".
		if (event.type == YAML_SCALAR_EVENT &&
				$strmatch ((char *) event.data.scalar.value, "hits"))
			break;
	}

	$expect_event (YAML_MAPPING_START_EVENT); // open curly
	$expect_event (YAML_SCALAR_EVENT);        // "total"
	$expect_event (YAML_SCALAR_EVENT);        // "total's" value
	$expect_event (YAML_SCALAR_EVENT);        // "max_score"
	$expect_event (YAML_SCALAR_EVENT);        // "max_score's" value
	$expect_event (YAML_SCALAR_EVENT);        // "hits"
	$expect_event (YAML_SEQUENCE_START_EVENT); // close curl

	while (1) {
		struct item item;
		int rt = parse_food_repo_food (&item);
		assert (!item.failed);
		if (!rt) arr_add (&r, item);
		else     break;
	}

	// No point parsing the rest of the document. Just stuff about API
	// versions.

	yaml_event_delete (&event);
	free (str);
	yaml_parser_delete (&parser);
	return r;
}

// Let's not use libyaml's "emitter" stuff. Maybe I'll want to later,
// but I suspect not.
static int write_food (FILE *f, struct item *item) {
	fprintf (f, "%s:\n", item->name);
	$fori (i, N_NUTRIENTS) {
		double *val = item->food.nutrient_vals_per_grams + i;
		if (*val != 0)
			fprintf (f, "  %s: %f\n", nutrients[i].name, *val);
	}
	return 0;
}

[[maybe_unused]]
static void make_names_unique (struct items *) {
}

int get_from_food_repo (char *food) {
	auto items = get_food_repo_response (food);

	if (items.failed) err (1, "Couldn't get items, but this message shouldn't happen");

	// We'll use review to print stuff, by making a meal and using
	// review. It doesn't feel right, but it's best to make use
	// of what we have. We do the same thing in main, when using
	// mt without -L.
	struct logfile_entries logfile_entries = {};
	struct meal meal = {};
	$fori (i, items.n)
		arr_add (&meal.bits,
				((struct meal_bit) {
					.type = MBT_FOOD,
					.special_size_i = -1,
					.multiplier = 1,
					.item_idx = i,
				}));
	push_meal_to_logfile_entries (&logfile_entries, meal, &items);

	review ("0s..1s", logfile_entries, review_mods, &items);

	char *rp = readline ("Enter a number to pick one: ");
	int *ints;
	size_t n_ints;
	struct s2ia_info info = s2ia_str_to_int_arr (&ints, &n_ints, rp, 0,
			items.n, 0);
	if (info.s2ia_errno)
		err (1, "%s", s2ia_error_strs[info.s2ia_errno]);
	free (rp);

	FILE *f = fopen ($food_file, "a");
	fputc ('\n', f);
	u_each (_, n_ints, ints) {
		write_food (f, items.d + *_);

		// Don't add a final line. Vim's yaml plugin doesn't like it.
		if (_ - ints != n_ints - 1)
			fputc ('\n', f);
	}
	fclose (f);

	if (args.flags & AF_EDIT)
		open_in_editor ();

	return 0;
}

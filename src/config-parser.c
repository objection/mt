#define _GNU_SOURCE

#include <string.h>
#include <assert.h>
#include <math.h>
#include <err.h>
#include <stdio.h>
#include "config-parser.h"
#include "../lib/useful/useful.h"
#include "all.h"
#include "../lib/libbmr/bmr.h"

int linenr = 1;
#define $err(fmt, ...) \
	errx (1, "%s/%s/" $config_file ":%d: " fmt, args.data_dir, args.profile_dir, \
			linenr, ##__VA_ARGS__);

static char *get_cmd_output (char *str) {
	char *r = 0;
	FILE *pipe = popen (str, "r");
	assert (pipe);
	size_t n = 0;
	if (-1 == getline (&r, &n, pipe))
		$err ("Couldn't get bmr from cmd \"%s\"", str);
	pclose (pipe);
	return r;
}

void parse_config_file (void) {
	size_t a = 0;
	char *buf = ({
		buf = 0;
		auto f = fopen ($config_file, "r");
		if (!f) return;

		getdelim (&buf, &a, 0, f) > 0

			// CONSIDER: I'm erroring here, on config file existing but
			// being empty. Maybe that should be a warning or silent.
			?: errx (1, "Couldn't get anything from " $config_file);
		fclose (f);
		buf;
	});

#define $key_is(val) \
	$strmatch (key, val)

	// Since I'm using strsep it's not that straightforward to get a
	// line counter. But -- genuinely -- there's gonna be about three
	// things in this config file, it -- genuinely -- does not matter.
	// A funny thing about this program. It has three parsers in it
	// and they all work differently. Silly, huh? But good practise.
#define $is_bad_err(fmt, ...) \
	errx (1, $config_file ": \"%s\" is bad %s. %s", val, key, #__VA_ARGS__);
#define $delims " \t"

	char *p = buf;
	struct bmr_details {
		double weight, age, height;
		enum bmr_sex sex;
	};
	struct bmr_details bmr_details = {
		.age = -1, .weight = -1, .sex = -1, .height = -1,
	};
	char val[BUFSIZ], key[BUFSIZ];
	config.bmr = -1;
	struct ts_time birthday = {};
	while (1) {
		char *_key = strsep (&p, $delims);
		char *_val = strsep (&p, "\n");
		if (!*_key)
			break;
		if (!*_val)
			$err ("Missing val for \"%s\"", key);
		strncpy (key, _key, BUFSIZ);
		strncpy (val, _val, BUFSIZ);

		// Your values can be the result of shell commands if you
		// prepend them with "!" or "|". This is really only so you
		// can get your "bmr" in some dynamic way. But there's no
		// reason why I can't let you do it for all variables.
		if (*val == '!' || *val == '|') {
			char *cmd_output = get_cmd_output (val + 1);
			strncpy (val, cmd_output, BUFSIZ);
			free (cmd_output);
		}
		if ($key_is ("bmr")) {
			if ($get_finite_double (&config.bmr, val, 0))
				$is_bad_err ("Should be float", val);

		} else if ($key_is ("birthday")) {
			birthday = ts_time_from_str (val, 0, &now,
					TS_MATCH_ONLY_ISO);

		} else if ($key_is ("age")) {
			if ($get_finite_double (&bmr_details.age, val, 0))
				$is_bad_err ("Should be float", val);

		} else if ($key_is ("height")) {
			if (bmr_parse_height (&bmr_details.height, val, 0))
				$is_bad_err ("Should be eg \"5ft3\" or \"1.8\" (meters))", val);

		} else if ($key_is ("weight")) {
			if (bmr_parse_weight (&bmr_details.weight, val, 0))
				$is_bad_err ("Should be eg \"16ts3\" or \"93\" (kg))", val);

		} else if ($key_is ("sex")) {
			if ($strncasematch (val, "female", 1))
				bmr_details.sex = BMR_SEX_F;
			else if ($strncasematch (val, "male", 1))
				bmr_details.sex = BMR_SEX_M;
			else
				$is_bad_err ("Valid values are \"female\" and \"male\"", val);
		} else if ($key_is ("weight-unit")) {

			// For empty weight unit unit, you do weight_unit = "" or
			// weight_unit = empty.
			if ($strmatch (val, "\"\"") || $strmatch (val, "empty"))
				config.weight_unit = "";
			else
				config.weight_unit = strdup (val);
		} else if ($key_is ("energy-unit")) {

			// For empty energy_unit = "" or weight_unit = empty.
			if ($strmatch (val, "\"\"") || $strmatch (val, "empty"))
				config.energy_unit = "";
			else
				config.energy_unit = strdup (val);
		} else if ($key_is ("default-grams")) {

			if (
					// args take priority.
					!isfinite (args.default_grams) &&
					sscanf (val, "%lf", &args.default_grams) != 1)
				$is_bad_err ("couldn't get default grams");

		} else if ($key_is ("bmr-adjustment")) {

			if (/* args take priority. */ !isfinite (args.bmr_adjustment) &&
				sscanf (val, "%lf", &args.bmr_adjustment) != 1)
					$is_bad_err ("couldn't get max-adjustment");
		} else
			$err ("Bad key, \"%s\"", key);
		linenr++;
	}

	free (buf);

	// If you've given "birthday", use that instead.
	if (birthday.ts != 0) {

		// FIXME: this isn't considering leap years.
		double secs = now.ts - birthday.ts;
		double secs_in_year = TS_SECS_FROM_YEARS (1);
		bmr_details.age = secs / secs_in_year;
	}
	if (!isfinite (args.bmr_adjustment))
		args.bmr_adjustment = 0;
	if (config.bmr == -1) { // If you've not specified bmr directly

		// If any one of these are set, all of them have to be.
		//
		// I have a feeling there's a simpler way to do this.
		// Also, libbmr itself could do this.
		if ((bmr_details.age == -1 || bmr_details.weight == -1 ||
					bmr_details.sex == -1 || bmr_details.height == -1) &&
				(bmr_details.age != -1 && bmr_details.weight != -1 &&
				 bmr_details.sex != -1 && bmr_details.height != -1)) {
			errx (1, $config_file "\
: You've set at least one of \"age\", \"sex\", \"weight\" and \"height\". \
But if you do one you need to do all");
		}
		config.bmr = bmr_calc (bmr_details.weight, bmr_details.height,
				bmr_details.age, bmr_details.sex);
	}
#undef $err
#undef $delims
#undef $is_bad_err
};

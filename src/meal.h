#pragma once

#include "all.h"

int set_meal (struct meal *pr, char *name, char *multiplier_or_special_size,
		char *multiplier, bool are_interactive, struct items *items);
int chop_meal_str (char **food, char **multiplier_or_special_size,
		char **multiplier, char *meal_str);
int parse_meal (struct item *pr, int n_strs, char **strs,
		char *name, bool are_interactive, struct items *items);
void push_meal_to_logfile_entries (struct logfile_entries *logfile_entries,
		struct meal meal, struct items *items);

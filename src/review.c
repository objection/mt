#define _GNU_SOURCE

#include "lib/rab/rab.h"
#include "lib/useful/useful.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "src/all.h"
#include "review.h"

#define ANSI_RED 				"\x1b[31m"
#define ANSI_YELLOW 			"\x1b[33m"
#define ANSI_NO_COLOUR 			"\x1b[0m"
enum { FIELD_MAX = 64 };
enum { NUMBER_MAX = 16 };

#define $is_reviewable_logfile_entry(_entry, _range) \
	(_entry->time.ts >= _range.s && \
	 \
		/* Exclusive end. Crucial! */ \
		_entry->time.ts < _range.e && \
		(!args.filter || !regexec (args.filter, _entry->item->name, 0, 0, 0)))

#define $number(buf, num) \
	({ \
		snprintf (buf, 16, "%.*f", num == round(num) ? 0 : 2, num); \
		buf; \
	})


char number_buf[NUMBER_MAX];
$make_arr (struct pt_record, table);

// I'd love to use ncmp for this, but I've discovered a fatal flaw in
// it. It's not possible as far as I know to make ncmp produce a
// function for cmping logfile_entry by food->name. Perhaps you could
// hack it together with varargs. But I dunno.
static int cmp_logfile_entry_by_name (const void *_a, const void *_b) {
	const struct logfile_entry *a = _a;
	const struct logfile_entry *b = _b;
	return strcasecmp (a->item->name,  b->item->name);
}

static struct logfile_entries get_grouped_logfile_entries (
		struct logfile_entries logfile_entries,
		struct ts_ts_range range) {

	struct logfile_entries r = {};
	arr_each (&logfile_entries, entry) {
		if (entry->time.ts < range.s

				// Exclusive end. Crucial!
				|| entry->time.ts >= range.e)
			continue;
		struct logfile_entry *grouped_entry = arr_find (&r, entry,
				cmp_logfile_entry_by_name);

		if (grouped_entry) {
			grouped_entry->meal_bit.multiplier += entry->meal_bit.multiplier;

			// Update the time so that if -t, what's printed
			// will be the last one. I could also, of course,
			// print all of the times in a line but that'd be
			// pointless effort.
			grouped_entry->time = entry->time;
		} else {
			struct logfile_entry new = *entry;
			arr_add (&r, new);
		}
	}
	return r;
}

struct pt_record get_day_record (struct ts_time day, int n_fields) {
	struct pt_record r = {.unformatted = 1};

	// This calloc's a bit of a guess.
	r.fields = calloc (n_fields, sizeof *r.fields);
	r.fields[0] = malloc (64 * sizeof *r.fields[0]);
	ts_str_from_time (r.fields[0], 64, &day, "%F");
	return r;
}

static time_t get_when_next_can_eat_timestamp (double cals_eaten_by_this_point,
		double cals_permitted_by_this_point,
		double whole_day_limit) {
	double cals_per_second = whole_day_limit / 86400.0;
	double over_by = cals_eaten_by_this_point - cals_permitted_by_this_point;
	double how_many_secs_until_you_can_eat = over_by / cals_per_second;
	return now.ts + how_many_secs_until_you_can_eat;
}

static void get_when_next_can_eat_str (size_t n_pr, char *pr,
		double cals_eaten_by_this_point, double cals_permitted_by_this_point,
		double whole_day_limit) {

	time_t when_next_can_eat_secs = get_when_next_can_eat_timestamp (cals_eaten_by_this_point,
			cals_permitted_by_this_point, whole_day_limit);

	if (when_next_can_eat_secs < now.ts) {
		strncpy (pr, "NOW", n_pr);
		return;
	}
	time_t a_day_from_now = now.ts + 86400;
	time_t start_of_the_day_after_that =
		ts_ts_with_units_set (a_day_from_now + 86400,
		(struct tm) {.tm_year = -1, .tm_mon = -1, .tm_mday = -1});
	char *fmt =
		when_next_can_eat_secs > start_of_the_day_after_that ?
			$iso_date :
			"%H:%M:%S";

	ts_str_from_ts (pr, n_pr, when_next_can_eat_secs, fmt);
}

static void get_countdown_str (size_t n_pr, char *pr,
		double cals_eaten_by_this_point, double cals_permitted_by_this_point,
		double whole_day_limit) {

	time_t when_next_can_eat_secs = get_when_next_can_eat_timestamp (cals_eaten_by_this_point,
			cals_permitted_by_this_point, whole_day_limit);

	// If you can already eat you'll get a negative dur. It might be
	// prettier to say "nh ago", but that's extra chars.
	ts_str_from_dur (pr, n_pr, &(struct ts_dur) {.secs = when_next_can_eat_secs - now.ts},
			&now, 0);
}

static char *get_warning_colour (double cal_decimal_frac) {
	if (args.review_mods.flags & RM_WARNING_THRESHOLDS) {
		if (cal_decimal_frac >= args.review_mods.warning_thresholds[0])
			return ANSI_RED;
		else if (cal_decimal_frac >= args.review_mods.warning_thresholds[1])
			return ANSI_YELLOW;
	}
	return "";
}

static void colour_string_if_over_warning_threshold (char *str, double cal_decimal_frac,
		enum print_flags print_flags) {
	if (print_flags & RM_WARNING_THRESHOLDS) {
		char *colour = get_warning_colour (cal_decimal_frac);
		rab_bookend (FIELD_MAX, str, "%s", colour, ANSI_NO_COLOUR);
	}
}

static char *get_no_colour (enum print_flags print_flags) {
	if (print_flags & RM_WARNING_THRESHOLDS)
		return ANSI_NO_COLOUR;
	return "";
}

static void get_supplemental_details_field (int n_pr, char *pr,
		enum print_flags print_flags, double cal_decimal_frac,
		double cals_eaten_by_this_point, double cals_permitted_by_this_point,
		double whole_day_limit) {

	if (!(print_flags & (RM_PRINT_PERCENT | RM_WHEN_NEXT_CAN_EAT |
					RM_COUNTDOWN))) {
		*pr = 0;
		return;
	}

	FILE *f = fmemopen (pr, n_pr * sizeof *pr, "w");
	assert (f);

	fprintf (f, " (");
	char buf[FIELD_MAX];
	int n_added = 0;
	if (print_flags & RM_PRINT_PERCENT) {
		fputs (get_warning_colour (print_flags), f);
		fprintf (f, "%s%%", $number (number_buf, cal_decimal_frac * 100));
		fputs (get_no_colour (print_flags), f);
		n_added++;
	}
	if (print_flags & RM_COUNTDOWN) {
		get_countdown_str (FIELD_MAX, buf,
				cals_eaten_by_this_point, cals_permitted_by_this_point,
				whole_day_limit);
		if (n_added)
			fputs (", ", f);
		fprintf (f, "%s", buf);
		n_added++;
	}
	if (print_flags & RM_WHEN_NEXT_CAN_EAT) {
		get_when_next_can_eat_str (FIELD_MAX, buf,
				cals_eaten_by_this_point, cals_permitted_by_this_point,
				whole_day_limit);
		if (n_added)
			fputs (", ", f);
		fprintf (f, "%s", buf);
		n_added++;
	}
	fputc (')', f);
	int rc = fclose (f);
	if (rc)
		err (1, "Couldn't make supplemental string: a programming error");

}

struct pt_record make_overview_record (double *totals,
		struct ts_ts_range range, int n_fields,
		struct review_mods review_mods) {
	struct pt_record r = {};
	r.fields = calloc (

			// I think I need to properly calloc the right number of
			// fields, for print_table, even though I'm not going to
			// use them.
			n_fields,
			sizeof *r.fields);
	int field_idx = 0;

#define $ovr_add(what) ({ r.fields[field_idx++] = what; })

	if (review_mods.flags & RM_PRINT_INDEX)
		$ovr_add (0);
	$ovr_add (strdup ("total"));
	$fori (i, review_mods.fields.n) {
		auto nutrient_id = review_mods.fields.d[i];
		double whole_day_limit = get_nutrients_funcs[nutrient_id]();
		time_t period = range.e - range.s;

		// 86400 == n secs in day.
		double day_fraction = args.review_str

			// You did -r or -R
			? period  / 86400.0

			// You used mt as a simple calculator: "mt cal:45
			// cal:100". This line means that if you also passed -%,
			// it'll print a percentage based on your BMR.
			: 86400.0 / 86400.0; // 1

		// I'm bad at maths and have a poor mathematical vocabulary so
		// these variable names are as descriptive as I can make them.
		// Except I consistently say "cals" when we're not necessarily
		// talkin about cals. But doing that makes it easier for me.
		double cals_permitted_by_this_point = whole_day_limit * day_fraction;

		double cals_eaten_by_this_point = totals[nutrient_id];
		double cal_decimal_frac = cals_eaten_by_this_point / cals_permitted_by_this_point;

		char cals_eaten_str[FIELD_MAX];
		char cals_permitted_str[FIELD_MAX];

		$number (cals_eaten_str, totals[i]);
		colour_string_if_over_warning_threshold (cals_eaten_str, cal_decimal_frac,
				review_mods.flags);

		$number (cals_permitted_str, cals_permitted_by_this_point);

		char supplemental_str[256];
		get_supplemental_details_field (256, supplemental_str,
				review_mods.flags, cal_decimal_frac, cals_eaten_by_this_point,
				cals_permitted_by_this_point, whole_day_limit);

		$ovr_add (rab_get_str ("%s/%s%s", cals_eaten_str, cals_permitted_str,
					supplemental_str));

	}

#undef $ovr_add
	return r;
}

struct pt_record make_name_record (int n_fields, struct review_mods review_mods,
		int max_portions) {
	struct pt_record r = {
		.fields = calloc (n_fields, sizeof *r.fields)
	};

	int field_idx = 0;

	// We'll strdup so we can free things cleanly.
#define $add(what) \
	({ r.fields[field_idx++] = strdup (what); })

	if (review_mods.flags & RM_PRINT_INDEX)
		$add ("idx");

	// We're just pointing to string literals here, and it's fine,
	// quite sure. They're in the binary.
	$add ("name");
	/* $add ("\"grams\""); */

	// Trickiness: In a moment we'll loop through the rest of the
	// nutrients, but we'll do "cal" here. Right now, the order is
	// fixed, and we always print cal, so it's fine. I could, I
	// suppose, make cal special, not part of the array, it's own
	// member in the struct. That might make things clearer that cal
	// is special. But it makes no difference. And I foresee cal being
	// not special at all, and the user being able to order the
	// fields any way they like.
	$add (nutrients[review_mods.fields.d[0]].name);

	if (review_mods.flags & RM_PORTIONS) {
		$fori (i, max_portions)
			$add (rab_get_str ("portion-%d", i + 1));
	}

	if (review_mods.flags & RM_PRINT_TIME)
		$add ("when");
	if (review_mods.flags & RM_PRINT_WHEN_SCHED)
		$add ("when-scheduled");

	// Trickiness: start from one, because we've already done "cal".
	for (int i = 1; i < review_mods.fields.n; i++)
		$add (nutrients[review_mods.fields.d[i]].name);
	return r;
}

double get_food_nutrient_total (enum nutrient_id nutrient_id, struct meal_bit meal_bit,
		struct item *item) {
	double grams = meal_bit.special_size_i == -1
		? 1
		: item->food.special_sizes.d[meal_bit.special_size_i].grams;
	return grams * item->food.nutrient_vals_per_grams[nutrient_id] * meal_bit.multiplier;
}

static double get_nutrient_total (struct item *item, struct meal_bit meal_bit,
		enum nutrient_id nutrient_id, struct items *items) {

	double r = 0;
	switch (item->type) {
		case IT_FOOD:
			r = get_food_nutrient_total (nutrient_id, meal_bit, item);
			break;
		case IT_MEAL:
			arr_each (&item->meal.bits, _) {
				auto this_item = items->d + _->item_idx;
				if (this_item->type == IT_MEAL)
					r += get_nutrient_total (this_item, *_, nutrient_id, items);
				else
					r += get_food_nutrient_total (nutrient_id,  *_, this_item);
			}
			r *= meal_bit.multiplier;
	}
	return r;
}

struct table get_table (struct logfile_entries logfile_entries,
		struct ts_ts_range range,

		// FIXME? I'm passing returning the total number of cals
		// through this parameter. Feels a bit lame, but the
		// alternative is looping a second time or doing shit that
		// involves pointless memory allocation.
		double *totals, int n_fields, struct review_mods review_mods,
		int max_portions, struct items *items) {
	char time_str[FIELD_MAX];
	struct table r = {};
	if (!logfile_entries.n) return r;
	struct ts_time_range current_day = {};

	// Just make an array of N_NUTRIENTS even though we'll only be
	// using up to review_mods.fields.n. This is to I don't have
	// to remember to call memset (you can't "{}" a VLA). It's just
	// cleaner. But it does mean I have to explain it here.
	double day_totals[N_NUTRIENTS] = {};

	int idx = 0;
	arr_add (&r, make_name_record (n_fields, review_mods, max_portions));
	arr_each (&logfile_entries, entry) {

		if (!$is_reviewable_logfile_entry (entry, range))
			continue;

		/* auto food = &entry->item->food; */
		auto item = entry->item;
		/* double *nutrient_vals_per_grams = item->food.nutrient_vals_per_grams; */
		struct pt_record record = {};
		record.fields = calloc (n_fields, sizeof *record.fields);

		// If you've done --print-days but not also --total, we great
		// a new record with just the date in it and push it on the
		// array. No need to check for AF_TOTAL because we don't get
		// here if both are set.
		if (review_mods.flags & RM_PRINT_DAYS &&
				entry->time.ts >= current_day.e.ts) {

			// Add the date, like: 2022-05-29

			// Here's where we set last_current_day. We're in a new
			// day. It's used below.
			memset (day_totals, 0, sizeof day_totals);
			current_day.s = ts_time_with_units_set (entry->time.ts,
					(struct tm) {.tm_year = -1, .tm_mon = -1, .tm_mday = -1});
			auto day_record = get_day_record (current_day.s, n_fields);
			current_day.e = ts_time_from_ts (current_day.s.ts + TS_SECS_FROM_DAYS (1));
			arr_add (&r, day_record);
		}

		// Name.
		// No need to malloc. No need to free, either. The program
		// exits at the end.
		int field_idx = 0;

#define $gt_add(what) ({ record.fields[field_idx++] = what; })

		if (review_mods.flags & RM_PRINT_INDEX)
			$gt_add (rab_get_str ("%zu", idx++));

		$gt_add (strdup (entry->item->name));

		double cals = get_nutrient_total (item, entry->meal_bit, review_mods.fields.d[0],
				items);
		$gt_add (strdup ($number (number_buf, cals)));

		if (review_mods.flags & RM_PORTIONS) {
			$fori (i, max_portions) {
				if (i >= item->food.special_sizes.n)
					$gt_add (strdup (""));
				else  {
					char *val = 0;
					asprintf (&val, "%s:%s", item->food.special_sizes.d[i].name,
							$number (number_buf, item->food.special_sizes.d[i].grams));
					$gt_add (val);
				}
			}
		}

		if (review_mods.flags & RM_PRINT_TIME) {

			if (!ts_str_from_ts (time_str, FIELD_MAX, entry->time.ts, $iso_date))
				errx (1, "Couldn't make --print-time time str");

			asprintf (&record.fields[field_idx++], "%s", time_str);

			if (review_mods.flags & RM_PRINT_WHEN_SCHED) {
				if (entry->scheduled_when.ts) {
					if (!ts_str_from_ts (time_str, FIELD_MAX, entry->scheduled_when.ts,
								$iso_date))
						errx (1, "Couldn't make --print-when-scheduled time str");
					asprintf (&record.fields[4], "%s", time_str);
				} else
					record.fields[field_idx++] = 0;
			}
		}

		$fori (i, review_mods.fields.n) {

			double cals = get_nutrient_total (entry->item, entry->meal_bit,
					review_mods.fields.d[i], items);
			if (i)
				asprintf (&record.fields[field_idx++], "%s",
						$number (number_buf, cals));
			day_totals[i] += cals;
			totals[i] += cals;
		}

		arr_add (&r, record);

		if (review_mods.flags & RM_PRINT_DAYS &&
				(entry - logfile_entries.d == logfile_entries.n - 1 ||
				(entry + 1)->time.ts >= current_day.e.ts))

			// Add the "overview": "Total cals ...", etc.

			arr_add (&r, make_overview_record (day_totals,
					(struct ts_ts_range) { .s = current_day.s.ts, .e = current_day.e.ts},
							n_fields, review_mods));
	}
	if (!(review_mods.flags & RM_NO_OVERVIEW))
		arr_add (&r, make_overview_record (totals, range, n_fields, review_mods));
	return r;
#undef $gt_add
}


// This is for --total and --print-days together.
//
// I could have this function be mixed in with get_table, but this is
// clearer. I tried it that way first and it was too twisty-turny.
// And also (2022-05-29T16:09:25+01:00), I really, really believe now
// that you're way better off just being longwinded and making
// separate code paths for separate things. The code'll be faster,
// clearer. The downside is changes need to be replicated in more
// places.
struct table get_day_total_table (struct logfile_entries logfile_entries,
		struct ts_ts_range range, double *totals, int n_fields,
		struct review_mods review_mods, struct items *items) {

	struct table r = {};

	arr_each (&logfile_entries, entry) {
		if (entry->time.ts < range.s

				// Exclusive end. Crucial!
				|| entry->time.ts >= range.e)
			continue;

		struct ts_time current_day_start = ts_time_with_units_set (entry->time.ts,
				(struct tm) {.tm_year = -1, .tm_mon = -1, .tm_mday = -1});

		double day_totals[N_NUTRIENTS] = {};
		while (entry < logfile_entries.d + logfile_entries.n &&
				entry->time.ts < (current_day_start.ts + TS_SECS_FROM_DAYS (1)) &&
				entry->time.ts >= range.s && entry->time.ts < range.e) {
			$fori (i, N_NUTRIENTS)
				day_totals[i] += get_nutrient_total (entry->item, entry->meal_bit,
						review_mods.fields.d[i], items);
			entry++;
		}
		entry--;
		struct pt_record record = {};
		record.fields = calloc (n_fields, sizeof *record.fields);

		// No need to malloc. No need to free, either -- we're
		// leaving.
		record.fields[0] = ts_str_from_time (0, 64, &current_day_start, "%F");

		$fori (i, review_mods.fields.n)
			asprintf (&record.fields[1 + i], "%s", $number (number_buf, day_totals[i]));

		arr_add (&r, record);
		$fori (i, review_mods.fields.n)
			totals[i] += day_totals[i];
	}
	return r;
}
static struct ts_ts_range get_today_ts_range (void) {
	struct tm set_to = {
		.tm_mon = -1,
		.tm_year = -1,
		.tm_mday = -1,
	};
	return (struct ts_ts_range) {
		.s = ts_ts_with_units_set (time (0), set_to),
		.e = ts_ts_with_units_set (time (0) + TS_SECS_FROM_DAYS (1), set_to),
	};
}

static struct ts_ts_range get_ts_range_od (char *range_str) {
	auto range =
		strstr (range_str, "today")

			// FIXME: at some point time_str needs to deal with
			// "variables" like "today", "tomorrow", itself.
			? get_today_ts_range ()
			: ts_ts_range_from_str (range_str, 0, &now, 0);

	if (ts_errno) errx (1, "%s", ts_strerror (ts_errno));
	return range;
}

static int get_n_portions_fields (struct logfile_entries logfile_entries,
		struct ts_ts_range range) {

	int r = 0;

	// Need to loop to get n_portions. It feels a bit lame. But
	// there's no other way. Well, there is, but this is simplest.
	// You could get a list of all matching logfile entries at the top
	// of review instead. that would be fine.
	arr_each (&logfile_entries, entry) {
		if ($is_reviewable_logfile_entry (entry, range) &&
				entry->item->food.special_sizes.n > r)
			r = entry->item->food.special_sizes.n;
	}

	return r;
}

int review (char *range_str, struct logfile_entries logfile_entries,
		struct review_mods _print_opts, struct items *items) {

	// review_mods are normally passed to this function, but they're
	// overridden by args.review_mods. So why bother passing
	// _print_opts? Answer: It feels weird to set args.review_mods
	// in get.c before calling this function; it feels weird to
	// within the program set a value in the args struct. That's used
	// for user options.
	auto review_mods = args.review_mods.flags & RM_HAVE ? args.review_mods : _print_opts;

	auto range = get_ts_range_od (range_str);
	int max_portions = review_mods.flags & RM_PORTIONS
			? get_n_portions_fields (logfile_entries, range)
			: 0;
	int n_fields = ({
		n_fields = 2; // Name, grams.
		n_fields += max_portions;
		if (review_mods.flags & RM_PRINT_TIME)
			n_fields += 1; // Time.
		if (review_mods.flags & RM_PRINT_WHEN_SCHED)
			n_fields += 1; // When Scheduled
		if (review_mods.flags & RM_PRINT_INDEX)
			n_fields += 1;
		if (review_mods.flags & RM_PORTIONS)
			n_fields += get_n_portions_fields (logfile_entries, range);
		n_fields += review_mods.fields.n;
		n_fields;
	});
	double totals[N_NUTRIENTS] = {};

	// As you see, this perhaps foolishly makes a new logfile_entries,
	// where each food type is only listed only once, and where the
	// cals grams field is the sum of all of the instances of that
	// meal. What a convoluted sentence.
	struct logfile_entries *entries = ({

		if (review_mods.flags & RM_GROUP) {

			// This is basically fine. Honestly. In the function we
			// just copy everything over. There is a pointer to item
			// inside the logfile_entry struct but we don't modify
			// anything in it. The "entries" variable here isn't
			// allocated memory; there's no memory leak. Obviously the
			// fact I have to explain and justify this suggests I
			// should do things in some other way.
			struct logfile_entries tmp = get_grouped_logfile_entries (logfile_entries,
					range);
			entries = &tmp;
		} else
			entries = &logfile_entries;
		entries;
	});

	int mask = RM_TOTAL | RM_PRINT_DAYS;
	auto records = (review_mods.flags & mask) == mask
		? get_day_total_table (*entries, range, totals, n_fields, review_mods, items)
		: get_table (*entries, range, totals, n_fields, review_mods, max_portions,
				items);

	// If you do just --total on its own you just get the total.
	if ((review_mods.flags & RM_TOTAL) && !(review_mods.flags & RM_PRINT_DAYS)) {
		$fori (i, review_mods.fields.n)
			printf ("%s\n", $number (number_buf, totals[i]));
		return 0;
	}

	pt_print_table (records.d, records.n, n_fields);

#ifdef $hilarious_joke
	if (range.e - range.s > TS_SECS_FROM_DAYS (15)
			&& (!records.n || totals[N_CAL] == 0))
		printf ("RIP ⚰\n");
#endif

	if (!records.n)
		return 1;

	arr_each (&records, _) {
		$fori (i, n_fields)
			$free_if (_->fields[i]);
		free (_->fields);
	}
	$free_if (records.d);
	return 0;
}

#include "all.h"
#include "../lib/useful/useful.h"
#include <math.h>

#define x(a, b, c, d) {.name = b},
struct nutrient nutrients[N_NUTRIENTS + 1] = { NUTRIENT };
#undef x
struct ts_time now;
char error_buf[4096];

struct args args = {
	.default_grams = NAN,
	.bmr_adjustment = NAN,
	.schedule = {.ts = -1},
	.review_mods = {
		.warning_thresholds = 0.8, 0.6,
		.flags = RM_WARNING_THRESHOLDS,
		.fields = {
			.d = {
				N_CAL,
			},
			.n = 1,
		}
	},
};

struct config config = {
	.weight_unit = "g",
	.energy_unit = "c"
};
$ncmp_def_by_member_using_func (special_size, name, char *, ncmp_strs);

void nuke_item (struct item *item) {
	free (item->name);
	memset (item, 0, sizeof *item);
}

int cmp_item_by_longest_name (const void *a, const void *_b, int n_a) {

	// Figure out the longest, so you get a proper match. If you just
	// choose, eg, arg, arg "chicken" will match food "chicken and
	// rice".

	const char **b = (const char **) _b;
	int n_b = strlen (*b);
	int n_longest = $max (n_a, n_b);

	return strncasecmp (*(const char **) a, *b, n_longest);
}

int cmp_item_by_name (const void *a, const void *_b) {

	// Figure out the longest, so you get a proper match. If you just
	// choose, eg, arg, arg "chicken" will match food "chicken and
	// rice".

	const char **b = (const char **) _b;

	return strcmp (*(const char **) a, *b);
}

void strip (char *s) {
	if (!s) return;
	char *p = s;
	int len = strlen (p);
	if (len == 0) return;

	while (isspace (p[len - 1])) p[--len] = 0;
	while (*p && isspace (*p)) ++p, --len;

	memmove (s, p, len + 1);
}

int cmp_nutrient_ids (void *a, void *b) {
	return *(enum nutrient_id *) a - *(enum nutrient_id *) b;
}

void open_in_editor (void) {
	char *editor;
	if ((editor = getenv ("EDITOR")) || (editor = getenv ("VISUAL"))) {
		char *cmd = rab_get_str ("%s %s", editor, $food_file);
		system (cmd);
		free (cmd);
	} else
		err (1, "\
Neither EDITOR or VISUAL set. Do, eg, export VISUAL=vim in your shell or call mt \
like (eg) this: EDITOR=vim mt ...");
}

#define _GNU_SOURCE

#include <err.h>
#include <assert.h>
#include <math.h> // IWYU pragma: keep
#include "src/all.h"
#include "../lib/useful/useful.h"
#include "meal.h"

static int dist (const char *s, const char *t, int ls, int lt, int *d, int i, int j) {

	// This function was once very prettily a nested in levenshtein,
	// but because clangd objected it had to become less prettily
	// its own man.
	// This was also nicked from
	// http://rosettacode.org/wiki/Levenshtein_distance#C
	if (d[i * (ls + 1) + j] >= 0)
		return d[i * (ls + 1) + j];

	int x;
	if (i == ls)
		x = lt - j;
	else if (j == lt)
		x = ls - i;
	else if (s[i] == t[j])
		x = dist (s, t, ls, lt, d, i + 1, j + 1);
	else {
		x = dist (s, t, ls, lt, d, i + 1, j + 1);

		int y;
		if ((y = dist (s, t, ls, lt, d, i, j + 1)) < x)
			x = y;
		if ((y = dist (s, t, ls, lt, d, i + 1, j)) < x)
			x = y;
		x++;
	}
	return d[i * (ls + 1) + j] = x;
}

// Nicked from http://rosettacode.org/wiki/Levenshtein_distance#C
static int levenshtein (const char *s, const char *t) {
	assert (s && t);
	int ls = strlen (s), lt = strlen (t);
	int d[ls + 1][lt + 1];
	memset (d, 0, sizeof (int) * ((ls + 1) * (lt + 1)));

	for (int i = 0; i <= ls; i++)
		for (int j = 0; j <= lt; j++)
			d[i][j] = -1;

	return dist (s, t, ls, lt, (int *) d, 0, 0);
}

// FIXME: better namefor this
typedef struct lev_entry {
	char *str;
	int distance;
} lev_entry;

static int cmp_lev_entries_by_distance (const void *a, const void *b) {
	return ((lev_entry *) a)->distance - ((lev_entry *) b)->distance;
}

static void get_top_levenshtein_distances (char **pr, size_t *n_pr, char *str,
		void *arr, size_t n_arr, size_t offset, size_t size) {
	int max_res = *n_pr;

	struct lev_entry entries[n_arr];
	/* memset (&entries, 0, sizeof *entr); */
	$fori (i, n_arr) {
		char *str_member = *((char **) (arr + (i * size)) + offset);
		entries[i].distance = levenshtein (str_member,  str);
		entries[i].str = str_member;
	}
	$qsort (entries, n_arr, cmp_lev_entries_by_distance);
	*n_pr = 0;
	$fori (i, max_res) {
		if (!entries[i].str || entries[i].distance >= 4)
			break;
		pr[(*n_pr)++] = entries[i].str;
	}
}

static void suggest_corrections (char *arg, void *arr, size_t n_arr,
		size_t offset, size_t size, bool are_interactive) {

	// If there's an error buf it means we're calling
	// "non-interactively" (it means wer've calling from
	// parse_foods.c.
	//
	// FIXME: there's no reason we can't do the levenshtein thing
	// here. I can't be bothered right now, because I'll have to
	// consider the buffer length.
	if (!are_interactive) {
		snprintf (error_buf, $n (error_buf), "There's no \"%s\"", arg);
		return;
	}
	size_t n_top = 3;
	char *top[n_top];
	memset (top, 0, n_top);
	get_top_levenshtein_distances (top, &n_top, arg, arr, n_arr, offset, size);

	// fmemopen seems to cause fclose to fail when you overwrite the
	// buffer. Probably because it sets the file error flag, or
	// whatever it calls. This makes fmemopen pretty cool.
	FILE *f = fmemopen (error_buf, $n (error_buf), "w");
	if (!f) err (1, "Couldn't write to buffer");
	fprintf (f, "There's no \"%s\"", arg);
	if (!n_top)
		fputc ('\n', stderr);
	else {
		fprintf (f, ". Try ");
		rab_print_comma_list (n_top, top,
				&(struct rab_pcl_opts) {.f = f, .and_str = "or"});
		fprintf (f, ", or check foods with --edit");
	}

	// Using errx here, even though fclose sets errno. It _doesn't_
	// seem to set errno with fmemopen. At least when you overrun the
	// buffer. I suspect what's happening is errno is getting eaten
	// up. If that's the case, I don't care about errno. The only
	// reason to use fmemopen here is the hope that it'll be a little
	// cleaner. And it's not cleaner if I check the buffer length
	// relentlessly. So many there's no silver bullet.
	!fclose (f) ?: errx (1, "\
Couldn't properly suggestions buffer. Probably overran the buffer");
}

// Your args are like "rice:100" or "rice:handfull:1". This
// returns whatever's after and turns the :
// into a zero, like strtok does.
static char *get_colon_subopt_place_zero (char *str) {
	if (!str) return 0;
	char *r = strchr (str, ':');
	if (r) {
		*r++ = 0;

		// Letting you put blanks between the : and the remaining
		// string.
		while (isblank (*r)) r++;
	}
	return r;
}

static int get_food_meal_bit (struct meal_bit *pr, int meal_bit_idx,
		char *name, char *multiplier_or_special_size, char *multiplier,
		bool are_interactive, struct items *items) {

	*pr = (struct meal_bit) {
		.item_idx = meal_bit_idx,
		.type = MBT_FOOD,
		.special_size_i = -1,
		.multiplier = 1,
	};
	auto item = items->d + meal_bit_idx;

	// In the yaml files, "size: " can leave you with an empty string.
	// This is fine, I think. We'll allow it.
	if (multiplier_or_special_size && *multiplier_or_special_size) {

		// If it's not a number it's a special size.
		if (!isdigit (*multiplier_or_special_size) &&
				*multiplier_or_special_size != '.' &&
				*multiplier_or_special_size != '-') {

			if (!item->food.special_sizes.n) {
				snprintf (error_buf, $n (error_buf),
						"\"%s\" has no special size", name);
				return 1;
			}

			struct special_size *special_size =
				arr_find (&item->food.special_sizes,
						&(struct special_size) {.name = multiplier_or_special_size},
						ncmp_special_sizes_by_name_using_ncmp_strs);

			// You've entered special size that doesn't exist.
			if (!special_size) {
				suggest_corrections (multiplier_or_special_size,
						item->food.special_sizes.d, item->food.special_sizes.n,
						offsetof (struct special_size, name),
						sizeof (struct special_size), are_interactive);
				return 1;
			}

			pr->special_size_i = special_size - item->food.special_sizes.d;

		} else {

			// Else it's just a straight grams. special_size_i == -1
			// means grams == 1, so all we need is the multiplier.
			pr->special_size_i = -1;
			if ($get_finite_double (&pr->multiplier,
						multiplier_or_special_size, 0)) {
				snprintf (error_buf, $n (error_buf), "\"%s\" isn't valid weight",
						multiplier_or_special_size);
				return 1;
			}
		}
	}

	// Get multiplier if it's there
	if (multiplier)
		if ($get_finite_double (&pr->multiplier, multiplier, 0)) {
			snprintf (error_buf, $n (error_buf), "\"%s\" isn't valid number of portions",
					multiplier);
			return 1;
		}

	return 0;
}

static int get_meal_meal_bit (struct meal_bit *pr, int meal_i, char *multiplier) {
	*pr = (struct meal_bit) {
		.item_idx = meal_i,
		.type = MBT_MEAL,
		.special_size_i = -1,
		.multiplier = 1,
	};
	if (multiplier) {

		// If it's not a number it's a special size.
		if (!isdigit (*multiplier) && *multiplier != '.' && *multiplier != '-') {
			sprintf (error_buf, "Meals can't have a special sizes");
			return 1;

		} else {

			// Else it's a multiplier
			pr->special_size_i = -1;
			if ($get_finite_double (&pr->multiplier, multiplier, 0)) {
				snprintf (error_buf, $n (error_buf), "\"%s\" isn't valid weight",
						multiplier);
				return 1;
			}
		}
	}

	return 0;
}

int set_meal (struct meal *pr, char *name, char *multiplier_or_special_size,
		char *multiplier, bool are_interactive, struct items *items) {

	/* int n_name = strlen (name); */

	struct item *item = u_find_by_member (items->d, items->n,
			&(struct item) {.name = name}, cmp_item_by_name, name);

	struct meal_bit meal_bit;

	if (!item) {

		suggest_corrections (name, items->d, items->n,
				offsetof (struct item, name), sizeof (struct item), are_interactive);
		return 1;
	}
	int r;
	switch (item->type) {
		case IT_FOOD: {
			r = get_food_meal_bit (&meal_bit, item - items->d, name,
					multiplier_or_special_size, multiplier, are_interactive, items);
		}
		break;
		case IT_MEAL: {
			r = get_meal_meal_bit (&meal_bit, item - items->d,
					multiplier_or_special_size);
		}
		break;
	}

	if (r) return 1;
	arr_add (&pr->bits, meal_bit);
	return r;
}

int chop_meal_str (char **food, char **multiplier_or_special_size,
		char **multiplier, char *meal_str) {

	// Split the arg. It might be "rice", and it might be
	// "rice:200".
	*food = meal_str;

	*multiplier_or_special_size = get_colon_subopt_place_zero (meal_str);

	*multiplier = get_colon_subopt_place_zero (*multiplier_or_special_size);

	strip (*food);
	strip (*multiplier_or_special_size);
	strip (*multiplier);
	return 0;
}

// "meal" is a whole commandline: "tatties:large coke:small".
int parse_meal (struct item *pr, int n_strs, char **strs,

		// The name is used when this is being called from
		// parse_foods.
		char *name, bool are_interactive, struct items *items) {

	// Number of meals parsed.
	int r = 0;
	// Doesn't print the last newline

	*pr = (struct item) {.name = name};
	u_each (arg, n_strs, strs) {

		// I'm quite proud of this. By reflex I pushed the non-option
		// args to an array, but just do this. It's not like you can
		// name a food with a leading "-". 2023-03-25T13:08:30+00:00:
		// I'll leave that previous comment here, because it's funny;
		// I was proud, but there is a bug here. It means that "mt -"
		// ends up being accepted. I mean, nothing happens, but an
		// error msg isn't printed.
		// What we had was "if (**arg == '-') continue;".
		if (*arg[0] == '-' && (*arg)[1] != 0)
			continue;

		char *food = 0, *multiplier_or_special_size = 0, *multiplier = 0;

		chop_meal_str (&food, &multiplier_or_special_size, &multiplier, *arg);
		!set_meal (&pr->meal, food, multiplier_or_special_size, multiplier,
				are_interactive, items) ?: ({
					errx (1, "%s", error_buf);
			});
		r++;

	}
	return r;
}

void push_meal_to_logfile_entries (struct logfile_entries *logfile_entries,
		struct meal meal, struct items *items) {

	// FIXME (never): The problem with this function is we're
	// recalculating things. We're doing what we did in the
	// get_meal_total function.

	arr_each (&meal.bits, _) {
		struct logfile_entry entry = {
			.item = items->d + _->item_idx,
			.time = args.schedule.ts != -1 ? args.schedule : now,
			.scheduled_when = args.schedule.ts != -1 ? now : (struct ts_time) {},
		};
		entry.meal_bit = *_;

		arr_add (logfile_entries, entry);
	}
}

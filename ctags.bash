#!/usr/bin/env bash

shopt -s globstar

gcc -M src/**/*.{c,h} lib/*/**/*.{c,h}  | sed -e 's/[\\ ]/\n/g' | \
        sed -e '/^$/d' -e '/\.o:[ \t]*$/d' | \
		ctags --extras=+q -L -


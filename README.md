# Meal Tracker

As the name suggests, this program will help you keep track of what
you've eaten.

## Examples

I'll start with examples. It really is the best way.

```
mt rice 					# You ate 100g of rice
mt rice:portion 			# You ate a "portion" of rice, which my
							# girlfriend says is 75g.

mt rice:portion:2  			# You ate two portions of rice
mt rice:portion:2.5 		# 2.5
mt --profile=drugs cig 		# You smoked a cigarette
mt -r2021-03-28..2021-03-29 # Print what you ate today
```

## Explanation

Without the -L flag, mt will simply calculate calories.

* `mt rice` might print "129.0".
* `mt rice potatoes` might print 203.

With the -L flag it'll do the same, but also write to a logfile
called "log" in (most likely) ~/.local/share/mt.

## Checking what you've eaten

You can check what you've eating with -r: `mt
-r-12h..0h`. The stuff after the r is a duration. The whole thing
means "print everything I've eaten between 12 hours ago and now (0h)."

In this case you'll get:

```
mt -r-24h..0h

tatties 350.00g 259.00c
Total cals: 259.00
```

Holy fuck, have I really eat nothing in 25h? I had to go looking for
bugs. But no, I just didn't record it. But what _did_ I eat? That's
why you've got to write things down as soon as possible.

Anyway, as you can see, there's the thing I ate, how many grams
(approximately) and how many calories the whole thing comes up to.

You can also specify dates like this: 2021-02-08 or
2021-02-08T13:53:03. You can't do, eg, "yesterday", but you can do
"today."

Another thing you can do with dates is this:

```

mt -r-1dT00..0d:00

```

This range encompasses yesterday. It means yesterday at 00 hours to
today at 00 hours.

## Config file

If you make a config file and stick it in your share dir (by default
~/.local/share/mt) you'll be able to see some other info when you use
--review.

That info might be:

```
Total cals: 60.00, 3.22% of your interval max (1863.38); headroom 1803.38
```

It's using your BMR (basal metabolic rate) to calculate this, which
it's getting from the config file.

If the file contains `bmr 2000`, then that's the value.

Otherwise you need to provide your sex, age, height and weight. You
can do "age", but you're better off doing "birthday", because then
you don't have to update it. Example:

```
birthday 1984-10-09
height 5ft11
weight 91.6
age 36
sex m
```

The "age" here will be ignored, because you did "birthday".

## Foods file

mt gets its food from a "foods" file, which should put in
~/.local/share/mt/foods. It could, no doubt, get it from
the Internet, but it doesn't. Here's some of my foods file.

```
potatoes	.74c  	quarter:43g half:93g
rice		1.29
indomie		5.0		pack:80g
onions		0.42
```

The first two fields are necessary. They're the name of the food and
how many calories are in it per gram. The name has to
be all one word: "chicken-and-rice", or "chicken\_and\_rice".

Whatever comes after it is a "portion". For example, in "potatoes",
I've said there's two portions, "quarter" and "half", and that a
"quarter" is 43g and a "half" is 93g. Of course, since potatoes vary
so much in size that's a pretty vague-sounding description. But _I_
know what I mean by quarter. Maybe that's a good reason to
compile your own foods file.

If you want to figure out cals-per-grams, go on something like
fatsecret.com or any of the other sites that are just like it.
I say "cals-per-grams", but you could also measure ml. It doesn't
matter, of course, what the unit is.

There's an example foods file in `./example-foods`. Don't expect
expect this to be really accurate. Some these foods I weighed,
some I got from duck-duckgoing.

## Positional parameters

The positional parameters are the foods, eg, "rice". But you can also
specify a weight (by default mt assumes your portion is 100g), like
this `mt rice:200`.

You can also specify one of the portions you define in the foods file,
like `mt potatoes:quarter`. You ate a quarter of a potato. Even if
you're looking after your weight you should eat a little more.

To eat 5 quarter potatoes, do `mt potatoes:quarter:5`. The five is
just a count, or multiplier. So you can do "potatoes:500:5", meaning 5
\* 500 grams.

## Logfile

Looks like this:

```
21-02-04T01:23:52 banana 150.00g 225.00c
21-02-04T10:33:22 indomie 80.00g 400.00c
21-02-04T20:55:22 galaxy 70.00g 348.60c
21-02-04T22:04:09 onions 338.00g 141.96c
21-02-04T22:04:09 peppers 86.00g 22.36c
```

First bits's the date and time you ate the thing. The next bits the
thing itself. Next bits's how much.

Last bits how many calories that adds up to. Because the logfile is
always read in and written back out, the calories will always use the
food's current cals-per-gram.

## "Profiles"

mt has "profiles", which are directories in XDG\_DATA\_HOME (or the
dir you provide with --data-dir), wherein you'll stick a foods file
and optionally a config file. The logfile ("log") will go there, too.

The default profile is called "default". Right now, mt won't try
to create it. This is because the directory's useless until you put a
at least a foods file in it.

You select a profile with --profile/-P.

The purpose of these is to count other things, for example "drugs"
like cigarettes and coffee.

```sh
mt -Pdrugs cigarette:2
```

If you do that, you'll want to see default-grams in the foods file,
like this "default-grams 1." Of course, we're not dealing with
"grams" any more but individual cigarettes, cups of coffee, etc.

## Installing

I should say this'll probably only work in Linux. And it only
compiles in gcc, because I've used nested functions.

You need Meson, which is in your repos.

Then clone the repo recursively, so you get the dependencies.

```sh
git clone --recursive https://gitlab.com/objection/mt.git
```

Then install.

```
meson build
ninja -Cbuild install
```

## Completion

These are in completion-functions.

### Bash completion

There's no Bash completion; reason being, getting Zsh's completion to
work took two whole days out of my life. It's seriously complicated,
though, as usual, once you know how, it's not that bad.

Bash's completion is simpler. If you want to write a script yourself,
search "complete", "REPLY", and "COMP_CWORDS" in the Bash manpage. Or,
better yet, look up a tutorial online.

### Zsh Completion

The completion script is completion-functions/_mt.

As I say at the bottom of this document, mt is a program that already
exists. And Zsh already has completion for it. So if you want to use
this, copy it into some directory of your choosing (perhaps
~/.zsh/completion-functions) and put something like this zshrc -- near
the top, before you `autoload -Uz compinit; compinit`.

```
fpath=(<some-directory-of-your-choosing> $fpath)
```

"fpath", by the way, is an array of places Zsh looks for what it calls
"functions" -- things it loads, and that can modify your Zsh
environment. The reason we put \<some-directory-of-your-choosing\>
_before_ fpath is Zsh seems to prioritise functions it reads first
over ones it reads later. That's different to how a lot of Unix
program and services are configured. completion-functions/_mt will be
used instead of the /usr/share/zsh/5.9/functions/_mt it finds later.

As for what to do if you actually use the proper mt program ... really
I should be good and rename this program. But for whom?

## Tips

### Sharing between computers

A program like this, you want it to use it from a number of computers.

You can do this with something like sshfs. That's why the
configuration stuff goes in ~/.local/share, not ~/.config, though I do
plan to let you put it there if you want.

Point is, remotely mount the directory, like I do, with sshfs. You can
stick something like this in /etc/fstab (without the line breaks). You
need to have exchange public keys to do this.

```fstab
me@remote-dir:/home/me/.local/share/mt /home/me/.local/share/mt fuse.sshfs x-systemd.automount,_netdev,user,idmap=user,transform_symlinks,identityfile=/home/me/.ssh/id_rsa,allow_other,default_permissions,uid=1000,gid=1000 0 0
```

### Generic cal

It's not too bad to have, as I do, to put a "generic-cal" entry in
your foods file, for those days you really can't be bothered. Also,
know that you can set the default grams, ie, the amount of grams mt
will count with when you just type the name of the food. Having said
that, I'm hardly counting actual grams these days. It's easier to say,
eg, `shroom:huge:4`.

## Bugs/misfeatures/non-features

* The logfile is always read in. If a logfile entry doesn't match,
  that's an error. This means if you change the name of a food in the
  foods file you need to do a search and replace in the logfile. Not a
  bug, but perhaps annoying.

* There's no pattern-matching. You can't do "chicken-and.\*" to mean
  "chicken and rice". This means longer names could be a pain to write
  down. I don't think I will put in pattern matching. Better, since
  part of the mt does it log, to be precise, I think. I'm gonna write
  a zsh completion function. I bet it's gonna be annoying.

* The name mt is already taken. That's really sad: I should have
  checked. mt's such a great name. Sounds ... sexy. Anyway, I'm not
  changing it (yet). Very antisocial. The real mt works on magnetic
  tapes, so hardly anyone's going to use it, right?

* I've completed fucked up the types. There should probably just be a
    struct food, and it can have children. Instead we have \"meals\",
    \"foods\", and \"items\", and they have pointers to each other. It
    makes my head spin. I'm typing here and revealing my shame because
    I want to shame myself. It's shameful.

* And it's so fucking awful. I want to just make the changes but my
    mind goes around and round when I do. The program works. It works
    fine. It doesn't matter if its programming can't understand why.
    He has the compiler on his side. I wonder if soft-language
    programmers are actually much cleverer than people like me. Maybe
    they're the kinds of people who think about things.

* It's just confusing. I think this is what people hate about
    object-orientated programming. This program isn't
    object-orientated. But it does have this annoying types issue. I'm
    not sure what the solution is. Well, I have developed a belief
    about programming, and it's that you should make your types pretty
    flat. You should avoid too much criss-crossing. But I also _do_
    think your types should be somewhat generic. Here, we should make
    things _more_ generic by getting rid of meals and just having
    foods. And they'd have children. I talked about this already.

* Yeah. If you just have "struct thing", and it has most things in it,
    you don't get criss-crossing. Only recursion. The problem is when
    you have two types that are similar but not the same, as is the
    case here. And when you have types that have other types in them,
    which my have pointers to objects of the first time. It's a
    nightmare. You go round and round in your head.
